/******************************
 FINALIDAD:

 Declarar las rutas del material, tanto del css como del js.
 Nos olvidaremos de quitar el '../../' de cada slide a la hora de subir al saleforce.

 USO:
 Antes de empezar hay que poner 2 líneas de código en el html:

 1) En el <head>: <script src="js/links.js"></script>
 2) Justo antes del </body>: <script>create_links_js();</script>

 Finalmente, asegurarnos de especificar todas las rutas en el primer link.js para luego copiar/pegar como dios manda en cada KM!

 USO ESPECIAL:
 Si queremos añadir una librería en un slide en concreto (por ejemplo, swiper.js), lo aplicaremos en 'link.js' de dicho slide.
 ******************************/




/******************************
 VARIABLES GLOBALES
 ******************************/

//Creates the corresponding link path for Salesforce/Veeva or localhost environments
var path_links = "content";

// Aquí declaramos todas los links css que se van a usar en toda la app
document.head.innerHTML += '<link rel="stylesheet" href=' + path_links + '/css/global.css>';
document.head.innerHTML += '<link rel="stylesheet" href=' + path_links + "/css/app.css>";
// document.head.innerHTML += '<link rel="stylesheet" href=' + path_links + "/slides/"+ currentSlide + '/css/espec.css>';



/**
 * Funcion que se llama desde el html para printar los js
 */
function create_links_js() {
    // Aquí declaramos todas los links js que se van a usar en toda la app
    document.write('<script src=' + path_links + '/js/libs/hammer.min.js></script>');
    document.write('<script src=' + path_links + '/js/libs/hammer-time.min.js></script>');
    document.write('<script src=' + path_links + '/js/libs/jquery-1.12.4.min.js></script>');
    document.write('<script src=' + path_links + '/js/libs/fastclick.js></script>');
    document.write('<script src=' + path_links + '/js/libs/tracking-lib.js></script>');
    document.write('<script src=' + path_links + '/js/libs/jquery.mark.es6.js></script>');
    document.write('<script src=' + path_links + '/js/libs/iscroll.js></script>');
    document.write('<script src=' + path_links + '/js/navegation.js></script>');
    document.write('<script src=' + path_links + '/js/ft.js></script>');
    document.write('<script src=' + path_links + '/js/global.js></script>');
    // document.write('<script src=' + path_links  + "/slides/"+ currentSlide + '/js/espec.js></script>');
}