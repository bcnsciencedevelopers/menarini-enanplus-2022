/*
 * Declaración de las slides que utilizará el eDetailing
 * 
 * active: si la slide está activa es true, si no es false.
 */
var slides = {
    1: {
        active: true,
    },
    2: {
        active: true,
    },
    3: {
        active: true,
    },
    4: {
        active: true,
    },
    5 : {
        active: true,
    },
    6 : {
        active: true,
    },
    7 : {
        active: true,
    },
    8 : {
        active: true,
    },
    9 : {
        active: true,
    },
    10 : {
        active: true,
    },
    11 : {
        active: true,
    },
    12 : {
        active: true,
    },
    13 : {
        active: true,
    },
    14 : {
        active: true,
    },
    15 : {
        active: true,
    },
    16 : {
        active: true,
    },
    17 : {
        active: true,
    },
    18 : {
        active: true,
    }
}

