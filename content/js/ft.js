/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var ft = `
        <div class="ft_text">

            <p class="ft_title">1. Nombre del medicamento </p>

            <p><span>Enanplus
                    75 mg/25 mg comprimidos recubiertos con película.</span></p>

            <p><span>Enanplus
                    75 mg/25 mg granulado para solución oral en sobre. </span></p>

            <p><span>&nbsp;</span></p>

            <p class="ft_title">2. Composición cualitativa y cuantitativa </p>

            <p><span>Cada comprimido contiene: 75 mg de
                    hidrocloruro de tramadol y 25 mg de dexketoprofeno. </span></p>

            <p><u><span>Excipientes con efecto conocido</span></u><span
                    >: cada comprimido
                    contiene 33,07 mg de croscarmelosa sódica y 1,83 mg de estearil fumarato
                    sódico. </span></p>

            <p><span>&nbsp;</span></p>

            <p><span>Cada sobre contiene: 75 mg de hidrocloruro
                    de tramadol y 25 mg de dexketoprofeno (como dexketoprofeno trometamol). </span></p>

            <p><u><span>Excipiente con efecto conocido:</span></u><span> 2,7 g de sacarosa por sobre</span></p>

            <p><span>&nbsp;</span></p>

            <p><span>Para consultar la lista completa de
                    excipientes ver sección 6.1.</span></p>

            <p><span>&nbsp;</span></p>

            <p class="ft_title">3. Forma farmacéutica </p>

            <p><u><span>Comprimidos recubiertos con película
                        (comprimidos)</span></u></p>

            <p><span>Enanplus: Comprimidos recubiertos con
                    película, casi blancos o ligeramente amarillos, ovalados, con una ranura en una
                    cara y una “M” inscrita en la otra cara. La dimensión de los comprimidos
                    recubiertos es de 14 mm de longitud y 6 mm de ancho. </span></p>

            <p><span>La ranura sirve únicamente para fraccionar
                    y facilitar la deglución pero no para dividir en dosis iguales.</span></p>

            <p>&nbsp;</p>

            <p><u><span>Granulado para solución oral en sobre</span></u></p>

            <p><span>Los gránulos son de color blanco o casi blanco. </span></p>

            <p><span>&nbsp;</span></p>

            <p class="ft_title">4. Datos clínicos </p>

            <p class="ft_title_2">4.1 Indicaciones terapéuticas </p>

            <p><span>Tratamiento
                    sintomático a corto plazo del dolor agudo de moderado a intenso en pacientes
                    adultos cuyo dolor requiera una combinación de tramadol y dexketoprofeno. </span></p>

            <p><span>&nbsp;</span></p>

            <p class="ft_title_2">4.2 Posología y forma de administración </p>

            <p class="ft_title_3"><u>Posología</u></p>

            <p><span>La dosis recomendada es de un comprimido recubierto con película o de un sobre
                    (correspondiendo a 75 mg de tramadol hidrocloruro y 25 mg de dexketoprofeno).
                    Pueden tomarse dosis adicionales cuando se requieran, con un intervalo mínimo
                    de dosificación de 8 horas. La dosis diaria total recomendada no debe
                    sobrepasar los tres comprimidos recubiertos con película o tres sobres al día
                    (que corresponde a 225 mg de tramadol hidrocloruro y 75 mg de dexketoprofeno).</span></p>


                <p><span>Enanplus
                    está destinado únicamente para uso a corto plazo y el tratamiento debe
                    limitarse estrictamente al periodo sintomático y en cualquier caso a no más de
                    5 días. Se deberá considerar cambiar a un único analgésico según la intensidad
                    del dolor y la respuesta del paciente.</span></p>


            <p><span>La aparición de
                    reacciones adversas puede minimizarse si se utilizan el menor número de dosis
                    durante el menor tiempo posible para controlar los síntomas (ver sección 4.4).</span></p>

            <p><span>&nbsp;</span></p>

            <p><i><span>Pacientes de edad avanzada:</span></i></p>

            <p><span>En pacientes de
                    edad avanzada la dosis inicial recomendada es de un comprimido recubierto con
                    película. Si es necesario, se pueden tomar dosis adicionales con un intervalo
                    mínimo de dosificación de 8 horas y sin exceder la dosis máxima diaria de 2
                    comprimidos recubiertos con película o  2 sobres (correspondiendo a 150 mg de
                    tramadol hidrocloruro y 50 mg de dexketoprofeno). La dosis puede incrementarse
                    hasta un máximo de 3 comprimidos recubiertos con película o 3 sobres al día,
                    tal y como se recomienda para la población general, sólo tras haber comprobado
                    una buena tolerabilidad general. </span></p>

            <p><span>Se dispone de
                    datos limitados en pacientes mayores de 75 años, por lo que Enanplus debe
                    usarse con precaución en estos pacientes (ver sección 4.4).</span></p>

            <p>&nbsp;</p>

            <p><i><span>Insuficiencia hepática:</span></i></p>

            <p><span>Los pacientes con
                    insuficiencia hepática de leve a moderada deben iniciar el tratamiento con un
                    número reducido de dosis (dosis diaria total de 2 comprimidos recubiertos con
                    película o 2 sobres de Enanplus) y deberán ser controlados cuidadosamente.</span></p>

            <p><span>No se debe
                    utilizar Enanplus en pacientes con insuficiencia hepática grave (ver sección
                    4.3).</span></p>

            <p><span>&nbsp;</span></p>

            <p><i><span>Insuficiencia renal:</span></i></p>

            <p><span
                    >En pacientes con
                    insuficiencia renal leve se debe reducir la dosis inicial total diaria a 2
                    comprimidos recubiertos con película o 2 sobres de Enanplus (aclaramiento de
                    creatinina 60 – 89 ml/min) (ver sección 4.4).</span></p>

            <p><span>No se debe
                    utilizar Enanplus en pacientes con insuficiencia renal de moderada a grave
                    (aclaramiento de creatinina ≤59 ml/ min) (ver sección 4.3). </span></p>

            <p>&nbsp;</p>

            <p><i><span>Población pediátrica:</span></i></p>

            <p><span>No se ha establecido la seguridad y
                    eficacia de Enanplus en niños y adolescentes. No se dispone de datos. </span></p>

            <p><span>Por ese motivo, no se debe utilizar Enanplus en niños y adolescentes.</span></p>

            <p>&nbsp;</p>

            <p class="ft_title_3"><u>Forma de administración</u></p>

            <p>Vía oral.</p>

            <p><u><span>Comprimidos recubiertos con película</span></u></p>

            <p><span>Enanplus se debe tragar con una cantidad suficiente de líquido (p.ej., un vaso de agua). </span></p>

            <p><u><span>Granulado para solución oral en sobre</span></u></p>

            <p><span>Disolver el total
                    del contenido de cada sobre en un vaso de agua; agitar/remover bien para ayudar
                    a disolver. La solución preparada es incolora, opalescente. La solución
                    obtenida debe tomarse inmediatamente tras su reconstitución.</span></p>

            <p><span>&nbsp;</span></p>

           

            <p><span>La administración
                    concomitante con alimentos retrasa la velocidad de absorción del fármaco (ver
                    sección 5.2), por lo que para un efecto más rápido de Enanplus debe tomarse al
                    menos 30 minutos antes de las comidas. </span></p>

            <p><span>&nbsp;</span></p>

            <p class="ft_title_2">4.3 Contraindicaciones </p>

            <p><span>Se deben tener en cuenta las
                    contraindicaciones descritas para dexketoprofeno y tramadol como principios
                    activos independientes. </span></p>

            <p><span>No se debe administrar dexketoprofeno en
                    los siguientes casos:</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    </span></span><span>hipersensibilidad
                    al dexketoprofeno, a cualquier otro AINE o a alguno de los excipientes
                    incluidos en la sección 6.1. </span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes
                    en los cuales sustancias con acción similar (p. ej., ácido acetilsalicílico, u
                    otros AINE) precipitan ataques de asma, broncoespasmo, rinitis aguda, o causan
                    pólipos nasales, urticaria o edema angioneurótico.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>reacciones
                    fotoalérgicas o fototóxicas conocidas durante el tratamiento con ketoprofeno o
                    fibratos.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes
                    con úlcera péptica/hemorragia gastrointestinal activa o con cualquier
                    antecedente de sangrado, ulceración o perforación gastrointestinal.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes
                    con antecedentes de hemorragia gastrointestinal o perforación relacionados con
                    tratamientos anteriores con AINE.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes con dispepsia crónica.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes
                    con otras hemorragias activas u otros trastornos hemorrágicos.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes
                    con la enfermedad de Crohn o colitis ulcerosa.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes con insuficiencia cardíaca grave.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes con insuficiencia renal moderada a grave (aclaramiento de creatinina ≤59 ml/min).</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes con insuficiencia hepática grave (puntuación de Child-Pugh C).</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes con diátesis hemorrágica y otros trastornos de la coagulación.</span></p>

            <p  style='
               '><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>pacientes
                    con deshidratación grave (causada por vómitos, diarrea o ingesta insuficiente
                    de líquidos).</span></p>

            <p><span>No se debe administrar tramadol en los
                    siguientes casos: </span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>hipersensibilidad
                    a tramadol o a alguno de los excipientes incluidos en la sección 6.1.</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>en
                    intoxicación aguda por alcohol, hipnóticos, analgésicos, opioides o
                    medicamentos psicótropos. </span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>en
                    pacientes en tratamiento con inhibidores de la MAO, o que los han tomado en los
                    últimos 14 días (ver sección 4.5).</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>en
                    pacientes con epilepsia que no esté controlada adecuadamente por el tratamiento
                    (ver sección 4.4).</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>depresión
                    respiratoria grave. </span></p>

            <p><span>Enanplus
                    está contraindicado durante el embarazo y la lactancia (ver sección 4.6).</span></p>

            <p><span>&nbsp;</span></p>

            <p class="ft_title_2">4.4 Advertencias y precauciones especiales de empleo </p>

            <p><span>Se deben tener en cuenta las advertencias
                    y precauciones descritas para dexketoprofeno y tramadol como principios activos
                    independientes. </span></p>

            <p><i><span>Dexketoprofeno</span></i></p>

            <p><span>Administrar con
                    precaución en pacientes con historia de condiciones alérgicas.</span></p>

            

            <p><span>Debe evitarse la
                    administración concomitante de dexketoprofeno con otros AINE incluyendo
                    inhibidores selectivos de la ciclooxigenasa-2 (ver sección 4.5).</span></p>

            

            <p><span>Puede reducirse la
                    aparición de efectos indeseables si se utiliza la dosis eficaz más baja durante
                    el menor tiempo posible para el control de los síntomas (ver sección 4.2 y
                    riesgos gastrointestinales y cardiovasculares más adelante).</span></p>

            

            <p><span>&nbsp;</span></p>

            <p><b><u><span>Seguridad gastrointestinal</span></u></b></p>


            <p><span>Se han descrito
                    hemorragias gastrointestinales, úlceras o perforaciones, que pueden ser
                    mortales, con todos los AINE en cualquier momento del tratamiento, con o sin
                    síntomas de previo aviso o antecedentes de acontecimientos gastrointestinales
                    graves. Deberá suspenderse el tratamiento con dexketoprofeno cuando ocurra una
                    hemorragia gastrointestinal o úlcera. </span></p>

            

            <p><span>El riesgo de
                    hemorragia gastrointestinal, úlcera o perforación es mayor a dosis de AINE
                    elevadas, en pacientes con historia de úlcera, sobre todo con hemorragia o
                    perforación (ver sección 4.3) y en pacientes de edad avanzada.</span></p>

          

            <p><span>Como en todos los
                    AINE, cualquier historia de esofagitis, gastritis y/o úlcera péptica debe ser
                    revisada para asegurar su total curación antes de iniciar el tratamiento con
                    dexketoprofeno trometamol. En los pacientes con síntomas gastrointestinales o
                    historia de enfermedad gastrointestinal, se debe vigilar la aparición de
                    trastornos gastrointestinales, especialmente hemorragia gastrointestinal.</span></p>

           

            <p><span>Los AINE se
                    administrarán con precaución en pacientes con historia de enfermedad
                    gastrointestinal (colitis ulcerosa, enfermedad de Crohn) ya que puede
                    exacerbarse su enfermedad (ver sección 4.8).</span></p>

            
            <p><span>En estos pacientes
                    y en los que requieren el uso concomitante de dosis bajas de ácido
                    acetilsalicílico o de otros fármacos que puedan incrementar el riesgo
                    gastrointestinal deberá considerarse la terapia combinada con agentes
                    protectores (p.ej. Misoprostol o inhibidores de la bomba de protones), (ver
                    siguiente y sección 4.5).</span></p>

          
            <p><span>Los pacientes con
                    historia de toxicidad gastrointestinal, en especial los pacientes de edad
                    avanzada, deberán comunicar cualquier síntoma abdominal inusual (especialmente
                    hemorragia gastrointestinal) sobretodo en las etapas iniciales del tratamiento.</span></p>

       

            <p><span
                    >Se aconsejará
                    precaución a los pacientes que reciben medicaciones concomitantes que puedan
                    incrementar el riesgo de úlcera o hemorragia, tales como corticosteroides
                    orales, anticoagulantes como warfarina, inhibidores selectivos de la
                    recaptación de serotonina o agentes antiagregantes como el ácido
                    acetilsalicílico (ver sección 4.5).</span></p>

            <p>&nbsp;</p>


            <p><b><u><span>Seguridad renal</span></u></b></p>

            <p><span>Se debe tener
                    precaución en pacientes con alteraciones de la función renal. En estos
                    pacientes, la utilización de AINE puede provocar un deterioro de la función
                    renal, retención de líquidos y edema. También se debe tener precaución en
                    pacientes que reciban diuréticos o en aquellos que puedan desarrollar
                    hipovolemia ya que existe un aumento del riesgo de nefrotoxicidad.</span></p>


            <p><span>Durante el
                    tratamiento se debe asegurar una ingesta adecuada de líquidos para prevenir
                    deshidratación y un posible aumento de la toxicidad renal asociada.</span></p>


            <p><span
                    >Como todos los
                    AINE puede elevar los niveles plasmáticos de nitrógeno ureico y de creatinina.
                    Al igual que otros inhibidores de la síntesis de las prostaglandinas, puede
                    asociarse a efectos indeseables del sistema renal que pueden dar lugar a
                    nefritis glomerular, nefritis intersticial, necrosis papilar renal, síndrome
                    nefrótico e insuficiencia renal aguda.</span></p>

            <p><span>&nbsp;</span></p>

            <p><b><u><span
                            >Seguridad hepática</span></u></b></p>

            <p><span
                    >Se debe tener
                    precaución en pacientes con alteraciones de la función hepática. Como otros
                    AINE, puede producir pequeñas elevaciones transitorias de alguno de los
                    parámetros hepáticos, y también incrementos significativos de la aspartato
                    aminotransferasa (AST), también conocida como transaminasa
                    glutámico-oxalacética sérica (SGOT), y de la alanina aminotransferasa (ALT),
                    también conocida como transaminasa glutámico-pirúvica sérica (SGTP). En caso de
                    un incremento relevante de estos parámetros deberá suspenderse el tratamiento.</span></p>

            <p><span
                    >&nbsp;</span></p>

            <p><b><u><span
                            >Seguridad
                            cardiovascular y cerebrovascular</span></u></b></p>

            <p><span
                    >Es necesario
                    controlar y aconsejar apropiadamente a los pacientes con historia de
                    hipertensión y/o insuficiencia cardíaca congestiva de leve a moderada, ya que
                    se ha notificado retención de líquidos y edema en asociación con el tratamiento
                    con AINE. Debe extremarse la precaución en pacientes con historia de
                    cardiopatía, en particular en pacientes con episodios previos de insuficiencia
                    cardíaca ya que existe un mayor riesgo de desencadenar un fallo cardíaco. </span></p>

            

            <p><span
                    >Datos procedentes
                    de ensayos clínicos y de estudios epidemiológicos sugieren que el empleo de
                    algunos AINE (especialmente en dosis altas y en tratamientos de larga duración)
                    se puede asociar con un pequeño aumento del riesgo de acontecimientos
                    aterotrombóticos (p. ej., infarto de miocardio o ictus). No existen datos
                    suficientes para poder excluir dicho riesgo en el caso de dexketoprofeno.</span></p>


            <p><span
                    >Los pacientes que
                    presenten hipertensión, insuficiencia cardiaca congestiva, cardiopatía
                    isquémica establecida, arteriopatía periférica y/o enfermedad cerebrovascular
                    no controladas sólo deberían recibir tratamiento con dexketoprofeno tras
                    considerarlo cuidadosamente. Debería realizarse una valoración similar antes de
                    iniciar un tratamiento de larga duración en pacientes con factores de riesgo de
                    enfermedad cardiovascular (p.ej., hipertensión, hiperlipidemia, diabetes mellitus,
                    fumadores).</span></p>

            

            <p><span>Todos los AINE no
                    selectivos pueden inhibir la agregación plaquetaria y prolongar el tiempo de
                    sangrado por inhibición de la síntesis de prostaglandinas. Por lo tanto, no se
                    recomienda el uso de dexketoprofeno en pacientes que reciban otras terapias que
                    puedan alterar la hemostasia, tales como warfarina u otros cumarínicos o
                    heparinas (ver sección 4.5).</span></p>

            <p>&nbsp;</p>

            <p><b><u><span>Reacciones cutáneas</span></u></b></p>

            <p><span>Muy raramente, y
                    asociadas al uso de AINE, se han comunicado reacciones cutáneas graves (algunas
                    de ellas mortales) que incluyen dermatitis exfoliativa, síndrome de
                    Stevens-Johnson y necrólisis epidérmica tóxica (ver sección 4.8). Parece que
                    los pacientes tienen un mayor riesgo de sufrir estos acontecimientos al inicio
                    del tratamiento; la aparición del acontecimiento ocurrió en la mayoría de los
                    casos durante el primer mes de tratamiento. Se debería interrumpir la
                    administración de dexketoprofeno tras la primera aparición de una erupción
                    cutánea, lesiones en las mucosas o cualquier otro signo de hipersensibilidad.</span></p>

            <p><span>&nbsp;</span></p>

            <p><b><u><span>Pacientes de edad avanzada</span></u></b></p>

            <p><span>Los pacientes de edad avanzada sufren una mayor incidencia de reacciones adversas a los AINE,
                    especialmente hemorragia y perforación gastrointestinal, que pueden ser
                    mortales (ver sección 4.2). Estos pacientes deberían iniciar el tratamiento con
                    la dosis más baja posible.</span></p>


            <p><span>Los pacientes de edad avanzada están más predispuestos a sufrir alteraciones de la función renal, cardiovascular o hepática (ver sección 4.2).</span></p>

            <p>&nbsp;</p>

            <p><b><u><span>Enmascaramiento de los síntomas de infecciones subyacentes</span></u></b></p>

            <p><span>Dexketoprofeno puede enmascarar los síntomas de una infección, lo que puede retrasar el inicio del tratamiento 
                adecuado y, por tanto, empeorar el desenlace de la infección. Esto se ha observado en la neumonía bacteriana 
                extrahospitalaria y en las complicaciones bacterianas de la varicela. Cuando se administre este medicamento para 
                aliviar el dolor relacionado con una infección, se recomienda vigilar la infección. En entornos no hospitalarios, 
                el paciente debe consultar a un médico si los síntomas persisten o empeoran.</span></p>

            <p><span>Excepcionalmente, la varicela puede ser el origen de complicaciones de infecciones cutáneas y de tejidos blandos 
            graves. Hasta la fecha, no se ha podido descartar el papel de los AINE en el empeoramiento de estas infecciones. Por ese 
            motivo, es recomendable evitar el uso de dexketoprofeno en caso de varicela.</span></p>

            <p>&nbsp;</p>

            <p><b><u><span>Otra información</span></u></b></p>

            <p><span>Se debe tener especial precaución en pacientes con:</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span>trastornos
                    congénitos del metabolismo de las porfirinas (p. ej. porfiria aguda
                    intermitente)</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >deshidratación</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >después de cirugía
                    mayor</span></p>



            <p><span>Muy raramente se han observado reacciones de hipersensibilidad aguda graves (p.
                    ej., shock anafiláctico). Debe interrumpirse el tratamiento ante los primeros
                    síntomas de reacciones de hipersensibilidad graves tras la toma de
                    dexketoprofeno. Dependiendo de los síntomas, cualquier procedimiento médico
                    necesario debe ser iniciado por profesionales sanitarios especialistas.</span></p>


            <p><span>Los pacientes con asma, combinado con rinitis crónica, sinusitis crónica, y/o
                    pólipos nasales tienen un mayor riesgo de sufrir alergia al ácido
                    acetilsalicílico y/o a los AINE que el resto de la población. La administración
                    de este medicamento puede provocar ataques de asma o broncoespasmo,
                    particularmente en pacientes alérgicos al ácido acetilsalicílico o a los AINE
                    (ver sección 4.3).</span></p>

            <!--<p><span>Excepcionalmente,
                    la varicela puede ser el origen de complicaciones de infecciones cutáneas y de
                    tejidos blandos graves. Hasta la fecha, no se ha podido descartar el papel de
                    los AINE en el empeoramiento de estas infecciones. Por ese motivo, es
                    recomendable evitar el uso de dexketoprofeno en caso de varicela.</span></p>-->



            <p><span>Se recomienda<b> </b>administrar con precaución dexketoprofeno en pacientes con
                    trastornos hematopoyéticos, lupus eritematoso sistémico o enfermedad mixta del
                    tejido conectivo.</span></p>



            <!--<p><span>Como otros AINE, dexketoprofeno puede enmascarar los síntomas de enfermedades
                    infecciosas.</span></p>-->

            <p><span>&nbsp;</span></p>



            <p><u><span>Población pediátrica</span></u></p>

            <p><span>La seguridad y eficacia de Enanplus en
                    niños y adolescentes no ha sido establecida. Por ese motivo, no se debe
                    utilizar Enanplus en niños y adolescentes. </span></p>

            <p><i>Tramadol</i></p>

            <p><span>Tramadol debe
                    administrarse con especial precaución en pacientes con dependencia, con traumatismo
                    craneal, shock, con nivel reducido de consciencia de origen desconocido,
                    trastornos del centro o de la función respiratoria o presión intracraneal
                    elevada. </span></p>

     

            <p><span
                    >En pacientes
                    sensibles a opioides, el medicamento debe administrarse con precaución.</span></p>


            <p><span
                    >Debe administrarse
                    con precaución en pacientes con depresión respiratoria, o si se administran
                    simultáneamente fármacos depresores del Sistema Nervioso Central (SNC) (ver
                    sección 4.5), o si se excede considerablemente la dosis recomendada (ver
                    apartado 4.9), ya que en estas situaciones no puede excluirse la posibilidad
                    que se produzca depresión respiratoria.</span></p>


            <p><span
                    >Se han notificado
                    convulsiones en pacientes tratados con tramadol en los niveles de dosificación
                    recomendados. Este riesgo puede aumentar si se excede el límite superior de la
                    dosis diaria recomendada (400 mg). </span></p>


            <p><span
                    >Adicionalmente,
                    tramadol puede incrementar el riesgo de convulsiones en pacientes que estén
                    recibiendo otra medicación que reduzca el umbral convulsivo (ver sección 4.5).
                    En pacientes epilépticos o pacientes susceptibles de sufrir convulsiones solo
                    se debe usar tramadol en circunstancias excepcionales. </span></p>

  

            <p><span
                    >Puede
                    desarrollarse tolerancia y dependencia psíquica y física, en especial después
                    del uso a largo plazo. En pacientes con tendencia al abuso o a la dependencia
                    de medicamentos, el tratamiento con tramadol sólo se debería llevar a cabo
                    durante períodos cortos y bajo estricto control médico. Cuando un paciente ya
                    no necesite tratamiento con tramadol, puede ser aconsejable reducir de forma
                    gradual la dosis para prevenir los síntomas de abstinencia.</span></p>

      
        <p><span>&nbsp;</span></p>

            <p><b><span>Riesgo por el uso
                        concomitante de medicamentos sedantes como benzodiacepinas o medicamentos
                        relacionados:</span></b></p>

            <p><span>El uso
                    concomitante de Enanplus y medicamentos sedantes como benzodiacepinas o
                    medicamentos relacionados puede provocar sedación, depresión respiratoria, coma
                    y muerte. Debido a estos riesgos, la prescripción concomitante con estos
                    medicamentos sedantes se debe reservar a pacientes para los que no son posibles
                    opciones alternativas de tratamiento. Si se decide la prescripción concomitante
                    de Enanplus con medicamentos sedantes, se debe usar la dosis efectiva más baja
                    y la duración del tratamiento debe ser lo más corta posible.</span></p>

            <p><span
                    >Se debe controlar
                    cuidadosamente a los pacientes para detectar signos y síntomas de depresión
                    respiratoria y sedación. En este sentido, se recomienda encarecidamente
                    informar a los pacientes y a sus cuidadores para que tengan en cuenta estos
                    síntomas (ver sección 4.5).</span></p>

            <p><span
                    >&nbsp;</span></p>

         
            <p><b>Síndrome serotoninérgico</b></p>

 

            <p><span>Se ha notificado
                    síndrome serotoninérgico, una enfermedad potencialmente mortal, en pacientes
                    tratados con tramadol en combinación con otros agentes serotoninérgicos o con
                    tramadol en monoterapia (ver secciones 4.5, 4.8 y 4.9).</span></p>

    

            <p><span
                    >Si el tratamiento
                    concomitante con otros agentes serotoninérgicos está clínicamente justificado,
                    se aconseja observar atentamente al paciente, especialmente en el momento de
                    iniciar el tratamiento y de aumentar las dosis. </span></p>



            <p><span
                    >Si se sospecha la
                    presenia de síndrome serotoninérgico, se considerará una reducción de la dosis
                    o la interrupción del tratamiento, en función de la gravedad de los síntomas.
                    La retirada de los medicamentos serotoninérgicos aporta por lo general una
                    rápida mejoría. </span></p>

            <p><span>&nbsp;</span></p>

           

            <p><b><span>Trastornos respiratorios relacionados con el sueño </span></b></p>

            <p><span>Los opioides
                    pueden causar trastornos respiratorios relacionados con el sueño, como la apnea
                    central del sueño (ACS) y la hipoxemia relacionada con el sueño. El uso de
                    opioides aumenta el riesgo de ACS en función de la dosis. En pacientes que
                    presenten ACS, se debe considerar disminuir la dosis total de opioides.</span></p>

            <p>&nbsp;</p>

            <p><b><span>Insuficiencia suprarrenal </span></b></p>

            <p><span
                    >Los analgésicos
                    opioides ocasionalmente pueden causar insuficiencia suprarrenal reversible que
                    requiere vigilancia y terapia de reemplazo de glucocorticoides. Los síntomas de
                    insuficiencia suprarrenal aguda o crónica pueden incluir p.ej. dolor abdominal
                    intenso, náuseas y vómitos, presión arterial baja, fatiga extrema, disminución
                    del apetito y pérdida de peso.</span></p>

            <p><span>&nbsp;</span></p>

            <p><b><span>Metabolismo del CYP2D6</span></b></p>

            <p><span
                    >El tramadol es
                    metabolizado por la enzima hepática CYP2D6. Si un paciente presenta una
                    deficiencia o carencia total de esta enzima, es posible que no se obtenga un
                    efecto analgésico adecuado. Los cálculos indican que hasta el 7 % de la
                    población de raza blanca puede presentar esta deficiencia. Sin embargo, si el
                    paciente es un metabolizador ultrarrápido, existe el riesgo de desarrollar
                    toxicidad por opioides, incluso a las dosis prescritas de forma habitual.</span></p>

            <p><span
                    >Los síntomas
                    generales de la toxicidad por opioides son confusión, somnolencia, respiración
                    superficial, pupilas contraídas, náuseas, vómitos, estreñimiento y falta de
                    apetito. En los casos graves, esto puede incluir síntomas de depresión
                    circulatoria y respiratoria, que puede ser potencialmente mortal y muy rara vez
                    mortal. Las estimaciones de prevalencia de metabolizadores ultrarrápidos en
                    diferentes poblaciones se resumen a continuación :</span></p>

            <p><span
                    >&nbsp;</span></p>

            <table Table border=1 cellspacing=0 cellpadding=0 class="ft_table">
                <tr>
                    <td valign="top">
                        <p><u><span style='color:black'>Población </span></u></p>
                        <p><span>Africana/etíope </span></p>
                        <p><span>Afroamericana </span></p>
                        <p><span>Asiática </span></p>
                        <p><span>Caucásica </span></p>
                        <p><span>Griega </span></p>
                        <p><span>Húngara </span></p>
                        <p><span>Europea del norte<u> </u></span></p>
                    </td>
                    <td valign="top">
                        <p><u><span style='color:black'>Prevalencia % </span></u></p>
                        <p><span>29 % </span></p>
                        <p><span>3,4 % a 6,5 % </span></p>
                        <p><span>1,2 % a 2 % </span></p>
                        <p><span>3,6 % a 6,5 % </span></p>
                        <p><span>6,0 % </span></p>
                        <p><span>1,9 % </span></p>
                        <p><span>1 % a 2 %<u> </u></span></p>
                    </td>
                </tr>
            </table>


            <p><span>&nbsp;</span></p>

            <p><b><span>Uso postoperatorio en niños</span></b></p>

            <p><span>En la bibliografía
                    publicada hay informes de que tramadol administrado en el postoperatorio a
                    niños después de una amigdalectomía y/o adenoidectomía por apnea obstructiva
                    del sueño provoca acontecimientos adversos raros, pero potencialmente mortales.
                    Se deben extremar las precauciones cuando se administre tramadol a niños para
                    el alivio del dolor postoperatorio y debe acompañarse de una estrecha
                    vigilancia de los síntomas de toxicidad por opioides, incluida depresión
                    respiratoria.</span></p>

            <p><b><span>&nbsp;</span></b></p>

            <p><b><span>Niños con
                        deterioro de la función respiratoria</span></b></p>

            <p><span>No se recomienda
                    el uso de tramadol en niños que puedan tener un deterioro de la función
                    respiratoria, incluidos trastornos neuromusculares, enfermedades cardíacas o
                    respiratorias graves, infecciones pulmonares o de las vías respiratorias altas,
                    traumatismo múltiple o que estén sometidos a procedimientos quirúrgicos
                    extensos. Estos factores pueden empeorar los síntomas de toxicidad por
                    opioides.</span></p>


            <p><span>Enanplus comprimidos
                    recubiertos con película contiene menos de 1 mmol de sodio (23 mg) por unidad
                    de dosis; esto es, esencialmente “exento de sodio”.</span></p>



            <p><span>Enanplus
                    granulado para solución oral contiene 2,7 g de sacarosa por dosis, esto debe
                    tenerse en cuenta en pacientes con diabetes mellitus. Los pacientes con
                    intolerancia hereditaria a la fructosa (IHF), problemas de absorción a la
                    glucosa o galactosa, o insuficiencia de sacarasa-isomaltasa, no deben tomar
                    este medicamento.</span></p>

            <p>&nbsp;</p>

            <p class="ft_title_2">4.5 Interacción con otros medicamentos y otras formas de
                interacción </p>

            <p><span>No se han realizado estudios clínicos para
                    evaluar el impacto potencial de las interacciones medicamento-medicamento en el
                    perfil de seguridad de Enanplus. Sin embargo, deben tenerse en cuenta las
                    interacciones notificados para dexketoprofeno y tramadol como principios
                    activos independientes. </span></p>

            <p><i><span>Dexketoprofeno</span></i></p>

            <p><span>Las siguientes interacciones son
                    aplicables a los antiinflamatorios no esteroideos (AINE) en general:</span></p>

 
            <p><i><u><span>Asociaciones no recomendadas</span></u></i><span>:</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span>Otros AINE
                    (incluyendo inhibidores selectivos de la ciclooxigenasa-2), incluyendo elevadas
                    dosis de salicilatos ( </span><span style='Symbol'>³</span><span
                    > 3 g/día): la
                    administración conjunta de varios AINE puede potenciar el riesgo de úlceras y
                    hemorragias gastrointestinales, debido a un efecto sinérgico.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span>
                    Anticoagulantes: 
                    los AINE pueden aumentar los efectos de los anticoagulantes, como la warfarina,
                    debido a la elevada unión del dexketoprofeno a proteínas plasmáticas y a la
                    inhibición de la función plaquetaria y al daño de la mucosa gastroduodenal. Si
                    no pudiera evitarse esta combinación, será necesario un estricto control
                    clínico y la monitorización de los parámetros analíticos. </span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Heparinas: existe
                    un aumento del riesgo  de hemorragia (debido a la inhibición de la función
                    plaquetaria y al daño de la mucosa gastroduodenal). Si no pudiera evitarse esta
                    combinación, será necesario un estricto control clínico y la monitorización de
                    los parámetros analíticos.</span></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Corticosteroides:
                    existe un aumento del riesgo de ulceración gastrointestinal o hemorragia (ver
                    sección 4.4).</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Litio (descrito
                    con varios AINE): los AINE aumentan los niveles del litio en sangre, los cuales
                    pueden alcanzar valores tóxicos (disminución de la excreción renal del litio).
                    Por tanto este parámetro requiere la monitorización durante el inicio, el
                    ajuste y la finalización del tratamiento con dexketoprofeno.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Metotrexato,
                    administrado a dosis elevadas, de 15 mg/semana o más: los antiinflamatorios en
                    general aumentan la toxicidad hematológica del metotrexato, debido a una disminución
                    de su aclaramiento renal.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Hidantoinas
                    (incluyendo fenitoína) y sulfonamidas: los efectos tóxicos de estas sustancias
                    pueden verse incrementados.</span></p>

            <p>&nbsp;</p>

            <p><i><u><span>Asociaciones que requieren precaución:</span></u></i></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Diuréticos,
                    inhibidores de la enzima de conversión de la angiotensina (IECA), antibióticos
                    aminoglucósidos y antagonistas de los receptores de la angiotensina II: dexketoprofeno
                    puede reducir el efecto de los diuréticos y de los antihipertensivos. En
                    algunos pacientes con compromiso de la función renal (p. ej., pacientes
                    deshidratados o pacientes de edad avanzada con compromiso de la función renal),
                    la combinación de agentes que inhiben la ciclooxigenasa e IECAs o antagonistas
                    de los receptores de angiotensina II o antibióticos aminoglucósidos puede
                    agravar el deterioro, normalmente reversible, de la función renal. Si se
                    combina dexketoprofeno y un diurético, deberá asegurarse que el paciente esté
                    hidratado de forma adecuada y deberá monitorizarse la función renal al
                    iniciarse el tratamiento de forma periódica. La administración concomitante de
                    dexketoprofeno con diuréticos ahorradores de potasio puede generar
                    hiperpotasemia. Se requiere monitorización de la concentración de potasio en
                    sangre (ver sección 4.4).</span></p>

            <p  style='
'><span
                    >-   Metotrexato,
                    administrado a dosis bajas, menos de 15 mg/semana: los antiinflamatorios en
                    general aumentan la toxicidad hematológica del metotrexato, debido a una
                    disminución de su aclaramiento renal. Durante las primeras semanas de la
                    terapia conjunta el recuento hematológico debe ser monitorizado semanalmente.
                    Se incrementará la vigilancia incluso en presencia de función renal levemente
                    alterada, así como en población de edad avanzada.</span></p>

            <p  style='
'><span
                    >-   Pentoxifilina:
                    aumento del riesgo de hemorragia. Se incrementará la vigilancia clínica y se
                    revisará el tiempo de sangrado con mayor frecuencia.</span></p>

            <p  style='
'><span
                    >-   Zidovudina: un
                    aumento del riesgo de toxicidad hematológica debido a la acción sobre los
                    reticulocitos, dando lugar a anemia severa a la semana del inicio del
                    tratamiento con el AINE. Se debe comprobar el recuento sanguíneo completo y el
                    recuento de reticulocitos una o dos semanas después del inicio del tratamiento
                    con el AINE.</span></p>

            <p>-   Sulfonilureas:
                los AINE pueden aumentar el efecto hipoglicemiante de las sulfonilureas por
                desplazamiento de los puntos de fijación a proteínas plasmáticas.</p>

            <p><span style='
                                                    '>&nbsp;</span></p>

            


            <p><i><u><span>Asociaciones a tener en cuenta:</span></u></i></p>

            <p  style='
'><span
                    >-   Beta-bloqueantes:
                    el tratamiento con un AINE puede disminuir su efecto antihipertensivo debido a
                    la inhibición de la síntesis de prostaglandinas.</span></p>

            <p  style='
'><span
                    >-    Ciclosporina
                    y tacrolimus: la nefrotoxicidad puede verse aumentada por los AINE debido a los
                    efectos mediados por las prostaglandinas renales. Debe controlarse la función
                    renal durante la terapia conjunta.</span></p>

            <p><span style='
                                                    '>-   Trombolíticos: aumento del riesgo de
                    hemorragia.</span></p>

            <p  style='
'><span
                    >-   Antiagregantes
                    plaquetarios e inhibidores selectivos de la recaptación de serotonina (ISRS):
                    aumentan el riesgo de hemorragia gastrointestinal (ver sección 4.4)</span></p>

            <p>-   Probenecid:
                puede aumentar las concentraciones plasmáticas de dexketoprofeno; esta
                interacción podría deberse a un mecanismo inhibitorio a nivel de la secreción
                tubular renal y de la glucuronoconjugación y requiere un ajuste de dosis del
                dexketoprofeno.</p>

            <p>-   Glucósidos
                cardiacos: los AINE pueden aumentar los niveles plasmáticos de los glucósidos
                cardíacos.</p>

            <p>-   Mifepristona:
                debido al riesgo teórico de que los inhibidores de la síntesis de
                prostaglandinas alteren la eficacia de la mifepristona, los AINE no deberían
                utilizarse en los 8 – 12 días posteriores a la administración de la
                mifepristona.</p>

            <p  style='
'><span
                    >Evidencias
                    científicas limitadas sugieren que la administración concomitante de AINE en el
                    día de administración de prostaglandinas no tiene un efecto perjudicial sobre
                    los efectos de mifepristona o las de  las prostaglandinas en la maduración
                    cervical o en la contractilidad uterina y que no reduce la eficacia de la interrupción
                    médica del embarazo.</span></p>

            <p>-   Quinolonas
                antibacterianas: datos en animales indican que altas dosis de quinolonas en
                combinación con AINE pueden aumentar el riesgo de convulsiones.</p>

            <p  style='
'><span
                    >-   Tenofovir: el
                    uso concomitante con AINE puede aumentar el nitrógeno ureico en plasma y la
                    creatinina. Deberá monitorizarse la función renal para controlar la influencia
                    sinérgica potencial en la función renal. </span></p>

            <p  style='
'><span
                    >-    Deferasirox:
                    el uso concomitante con AINE puede aumentar el riesgo de toxicidad
                    gastrointestinal. Se requiere un estricto control clínico cuando se combina
                    deferasirox con estas sustancias.  </span></p>

            <p  style='
'><span
                    >-    Pemetrexed:
                    la combinación con AINE puede disminuir la eliminación de pemetrexed, por ese
                    motivo se debe tener precaución al administrar altas dosis de AINEs. En
                    pacientes con insuficiencia renal de leve a moderada (aclaración de creatinina
                    de 45 a 79 ml/min), se debe evitar la administración conjunta de pemetrexed con
                    AINE durante 2 días antes y 2 después de la administración de pemetrexed.  </span></p>

            

                    <p>&nbsp;</p>

            
            <p><i><span>Tramadol</span></i></p>

            <p><i><u><span>Asociaciones no recomendadas:</span></u></i></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Tramadol no debe
                    combinarse con inhibidores de la Monoamino Oxidasa (MAO) (ver sección 4.3). Se
                    han observado interacciones que ponen en peligro la vida del paciente y que
                    afectan al sistema nervioso central, a la función respiratoria y cardiovascular
                    cuando se administran inhibidores de la MAO en los 14 días previos a la
                    utilización del opioide petidina. </span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Se debe tener
                    precaución durante el tratamiento concomitante con tramadol y derivados
                    cumarínicos (p. ej., warfarina) ya que se han notificado casos de aumento del
                    INR (International Normalized Ratio) con hemorragias mayores y equimosis en
                    algunos pacientes.</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >La combinación de
                    agonistas/antagonistas mixtos de receptores opioides (p. ej., bupronorfina,
                    nalbufina, pentazocina) con tramadol, no es aconsejable ya que, teóricamente,
                    el efecto analgésico de un agonista puro puede ser reducido en estas
                    circunstancias.  </span></p>

            <p  style='
'><i><u><span
                            >Asociaciones que
                            requieren precaución:</span></u></i></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Tramadol puede
                    provocar convulsiones e incrementar el potencial de originar convulsiones de
                    los inhibidores selectivos de la recaptación de serotonina (ISRS), inhibidores
                    de la recaptación de serotonina-norepinefrina (IRSN), de antidepresivos
                    tricíclicos, antipsicóticos y de otros medicamentos que reducen el umbral
                    convulsivo (p. ej., como bupropion, mirtazapina, tetrahidrocanabinol). </span></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >El uso terapéutico
                    concomitante de tramadol con otros medicamentos serotoninérgicos, como los
                    inhibidores selectivos de la recaptación de serotonina (ISRS), inhibidores de
                    la recaptación de serotonina-norepinefrina (IRSN), inhibidores de la MAO (ver
                    sección 4.3), antidepresivos tricíclicos, y mirtazapina, puede causar </span>un
                síndrome serotoninérgico potencialmente mortal (ver secciones 4.4 y 4.8<span
                    > </span></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >El uso
                    concomitante de opioides con medicamentos sedantes como benzodiacepinas o
                    fármacos relacionados aumenta el riesgo de sedación, depresión respiratoria,
                    coma y muerte debido al efecto depresor aditivo del SNC. Se debe limitar la
                    dosis y la duración del uso concomitante (ver sección 4.4).</span></p>

            <p><i><u><span style='
'>Asociaciones a tener en cuenta:</span></u></i></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >La administración
                    concomitante de tramadol con otros medicamentos depresores del sistema nervioso
                    central o con alcohol puede potenciar los efectos sobre el sistema nervioso
                    central (ver sección 4.8). </span></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Los resultados de
                    los estudios farmacocinéticos han mostrado que no se esperan interacciones
                    clínicas relevantes con la administración concomitante o previa de cimetidina
                    (inhibidor enzimático). </span></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >La administración
                    previa o simultanea de carbamazepina (inductor enzimático) puede reducir el
                    efecto analgésico y reducir la duración de la acción.    </span></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >En un limitado
                    número de estudios la administración pre o posoperatoria del antiemético
                    antagonista 5-HT3 ondansetrón, ha incrementado el requerimiento de tramadol en
                    pacientes con dolor posoperatorio.    </span></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                    >Otros medicamentos
                    conocidos que inhiben el CYP3A4, como ketoconazol y eritromicina, podrían
                    inhibir el metabolismo de tramadol (N-desmetilación) y probablemente también el
                    metabolismo del metabolito activo O-desmetilado. No se ha estudiado la
                    importancia clínica de este tipo de interacción.   </span></p>

                    <p>&nbsp;</p>
                    
            <p class="ft_title_2">4.6 Fertilidad, embarazo y lactancia </p>

            <p class="ft_title_3"><u>Embarazo</u></p>

            <p><span>No se han dado
                    casos de embarazo durante el desarrollo clínico de Enanplus. En los estudios
                    clínicos incluidos en esta sección no se ha establecido un perfil de seguridad
                    de Enanplus durante el embarazo. Se deben considerar los datos notificados para
                    dexketoprofeno y tramadol como principios activos independientes.    </span></p>

            <p><span>&nbsp;</span></p>

            <p><i><span>Dexketoprofeno</span></i></p>

            <p><span>
            La inhibición de la síntesis de prostaglandinas puede afectar de forma adversa al embarazo y/o
                    al desarrollo embrio-fetal. Datos de estudios epidemiológicos sugieren un
                    aumento del riesgo de aborto y de malformación cardiaca y gastrosquisis después
                    de utilizar inhibidores de la síntesis de prostaglandinas al principio del
                    embarazo. El riesgo absoluto de malformaciones cardiovasculares se incrementó
                    en menos del 1%, hasta aproximadamente el 1,5%. Se cree que el riesgo aumenta
                    en función de la dosis y de la duración de la terapia. En animales, la
                    administración de inhibidores de la síntesis de prostaglandinas ha mostrado un
                    aumento de pérdidas pre- y post- implantación y de letalidad embrio-fetal.
                    Además, se ha notificado una mayor incidencia de diferentes malformaciones,
                    incluyendo cardiovasculares, en animales a los que se administró un inhibidor
                    de la síntesis de prostaglandina durante el periodo organogénico. No obstante,
                    los estudios realizados en animales a los que se ha administrado dexketoprofeno
                    no mostraron toxicidad reproductiva (ver sección 5.3).</span></p>

            <p><span>
            A partir de la semana 20 de embarazo, el uso de dexketoprofeno puede provocar oligohidramnios como resultado 
            de una disfunción renal fetal. Esto puede ocurrir poco después del inicio del tratamiento y habitualmente es 
            reversible mediante la interrupción de este. Además, se han notificado casos de constricción del conducto arterioso 
            tras el tratamiento durante el segundo trimestre, la mayoría de los cuales se resolvieron tras el cese del tratamiento.
            </span></p>
            

            <p>&nbsp;</p>

            <p><span>Durante
                    el tercer trimestre de embarazo, todos los inhibidores de la síntesis de
                    prostaglandinas pueden exponer el feto a:</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>
            <span>toxicidad cardiopulmonar (constricción/ cierre prematuro del ductus arteriosus e hipertensión pulmonar);</span></p>

            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>
            <span>disfunción renal (véase más arriba);</span></p>

            <p>&nbsp;</p>
            
            <p><span
                    >Al final del
                    embarazo, la madre y el recién nacido pueden exponerse a:</span></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>una
                    posible prolongación del tiempo de sangrado y efecto antiagregante, que puede
                    producirse incluso a dosis muy bajas,</span></p>

            <p  style='

'><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span>una
                    inhibición de contracciones uterinas, que daría lugar a un retraso o
                    prolongación del parto.</span></p>

            <p><span
                    >&nbsp;</span></p>

            <p><i><span>Tramadol</span></i></p>

            <p  style='
'><span>Estudios
                    con tramadol en animales a muy altas dosis revelaron efectos en el desarrollo
                    de los órganos, osificación y mortalidad neonatal. No se observaron efectos
                    teratogénicos. Tramadol atraviesa la barrera placentaria. No existe una
                    evidencia adecuada sobre la seguridad de tramadol en el embarazo humano.</span></p>

            <p><span
                    >&nbsp;</span></p>

            <p><span
                    >Tramadol, cuando
                    se administra antes o durante el parto, no afecta la contractibilidad uterina.
                    En el recién nacido puede inducir alteraciones de la frecuencia respiratoria
                    que en general no tienen relevancia clínica. El uso crónico durante el embarazo
                    puede dar lugar a síndrome de abstinencia neonatal.</span></p>

           

            <p  style='
                
'><span>Considerando lo anterior, Enanplus está contraindicado en el embarazo (ver sección 4.3). </span></p>

            <p  style='
                
'><i><span>&nbsp;</span></i></p>

            <p class="ft_title_3"><u>Lactancia</u></p>

            <p><span>No se han realizado estudios controlados sobre la excreción de Enanplus en leche
                    humana. Se deben considerar los datos notificados para dexketoprofeno y
                    tramadol como principios activos independientes. </span></p>

            <p>&nbsp;</p>

            <p><i><span>Dexketoprofeno</span></i></p>

            <p><span>Se desconoce si el dexketoprofeno se excreta en la leche materna.</span></p>

            <p><span>&nbsp;</span></p>

            <p><i><span>Tramadol</span></i></p>

            <p><span>Se encuentran
                    pequeñas cantidades de tramadol y sus metabolitos  en la leche materna humana.</span></p>

            <p><span>Aproximadamente,
                    el 0,1% de la dosis materna de tramadol se excreta en la leche materna. En el
                    período inmediatamente posterior al parto, para dosis diarias orales maternas
                    de hasta 400 mg, esto se corresponde a una cantidad media de tramadol ingerida
                    por lactantes del 3 % de la dosis materna ajustada al peso. Por este motivo, no
                    debe utilizarse tramadol durante la lactancia o, como alternativa, debe
                    interrumpirse la lactancia durante el tratamiento con tramadol. Por lo general,
                    no es necesario interrumpir la lactancia después de una dosis única de
                    tramadol.</span></p>



            <p><span>Considerando lo anterior, Enanplus está contraindicado durante la lactancia (ver sección
                    4.3).</span></p>

            <p><i><span>&nbsp;</span></i></p>

            <p class="ft_title_3"><u>Fertilidad</u></p>

            <p><span>Como
                    otros AINE, el uso de dexketoprofeno puede alterar la fertilidad femenina y no
                    se recomienda en mujeres que están intentando concebir. En mujeres con
                    dificultades para concebir o que están siendo sometidas a un estudio de
                    fertilidad, se debería considerar la suspensión de este medicamento. </span></p>

            <p><span>&nbsp;</span></p>

            <p class="ft_title_2">4.7 Efectos sobre la capacidad para conducir y utilizar
                máquinas </p>

            <p><span>Los efectos conocidos para los componentes
                    de Enanplus de forma independiente, aplican a la combinación fija de los mismos.</span></p>

            <p>&nbsp;</p>

            <p><i><span>Dexketoprofeno</span></i></p>

            <p><span>La influencia de Enanplus sobre la
                    capacidad para conducir y utilizar máquinas es pequeña o moderada, debido a la
                    posible aparición de mareos o somnolencia. </span></p>

            <p>&nbsp;</p>

            <p><i><span>Tramadol</span></i></p>

            <p><span>Tramadol puede causar efectos como
                    somnolencia y mareos, incluso si se usa de acuerdo a las instrucciones de uso.
                    Por ese motivo, puede perjudicar las reacciones de los conductores y operarios
                    de máquinas. </span></p>



            <p><span>Esto sucede particularmente en combinación
                    con otras sustancias psicótropas y con alcohol. </span></p>

            <p><span>&nbsp;</span></p>



            <p class="ft_title_2">4.8 Reacciones adversas </p>

            <p><span>Los acontecimientos
                    adversos notificados como al menos posiblemente relacionados en los estudios
                    clínicos realizados con Enanplus y las reacciones adversas notificadas en las
                    fichas técnicas de las formulaciones orales  de dexketoprofeno y tramadol se
                    relacionan a continuación, clasificados por clases de sistemas de órganos: </span></p>

            <p><span>&nbsp;</span></p>

            <p><span>Las frecuencias se
                    describen de la siguiente manera:</span></p>

            <p><span>Muy frecuentes: ≥
                    1/10</span></p>

            <p><span>Frecuentes: ≥
                    1/100 a &lt;1/10</span></p>

            <p><span>Poco frecuentes:
                    ≥1/1000 a &lt;1/100</span></p>

            <p><span>Raras: ≥ 1/10.000
                    a &lt;1/1000</span></p>

            <p><span>Muy raras: &lt;
                    1/10.000</span></p>

            <p><span>Desconocidas: no
                    es estimable a partir de los datos disponibles</span></p>

            <p><span>&nbsp;</span></p>



            <table Table border=1 cellspacing=0 cellpadding=0 class="ft_table">
                <tr>
                    <td rowspan="2" valign="top">
                        <p><b><span>CLASIFICACIÓN MedDRA POR ÓRGANOS Y SISTEMAS</span></b></p>
                    </td>
                    <td rowspan="2" valign="top">
                        <p><span>Reacción adversa</span></p>
                    </td>
                    <td colspan=3 valign="top">
                        <p><span>Frecuencia</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Enanplus</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Dexketoprofeno </span></p>
                    </td>
                    <td valign="top">
                        <p><span>Tramadol</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" valign="top">
                        <p><span 
                    >Trastornos de la sangre y del sistema linfático</span></p>
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Trombocitosis</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Neutropenia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>-</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>-</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Trombocitopenia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>-</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>-</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" valign="top">
                        <p><span 
                    >Trastornos del sistema inmunológico</span></p>
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Hipersensibilidad
                                (p.ej., disnea, broncoespasmo,  sibilancias, angioedema)</span></p>
                    </td>
                    <td valign="top">
                        <p><span>-</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Reacciones
                                anafilácticas, incluyendo shock anafiláctico.   </span></p>
                    </td>
                    <td valign="top">
                        <p><span>-</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Edema laríngeo</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>Rara</span></p>
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>-</span></p>
                    </td>
                </tr>
                <tr>
                    <td  rowspan=4 valign="top">
                        <p><span>Trastornos del metabolismo y de la nutrición</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>Trastorno del
                                apetito</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara </span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Disminución del apetito</span></p>
                    </td>
                    <td valign="top">
                        <p><span>-</span></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>Rara</span></p>
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>-</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Hipoglucemina</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Desconocida</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Hipopotasemia </span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p align="left"><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan=10 valign="top">
                        <p><span>Trastornos psiquiátricos</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>Ansiedad</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Trastornos cognitivos</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Estado de confusión</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Dependencia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Alucinaciones</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Insomnio</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Alteración del ánimo</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Pesadillas</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Alteraciones psicóticas</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente </span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Alteración del sueño</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan=12 valign="top">
                        <p><span>Trastornos del sistema nervioso</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Coordinación anormal</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Mareos</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Epilepsia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Dolor de cabeza</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Contracciones musculares involuntarias </span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Parestesias</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Trastornos sensoriales</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Síndrome serotoninérgico</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Desconocida</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Somnolencia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top"'>
                        <p><span>Frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Alteración del habla</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Desconocida</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Sincope</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Temblor</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                

                <tr>
                    <td rowspan=4 valign="top">
                        <p><span>Trastornos oculares</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Visión borrosa</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Midriasis</span></p>
                    </td>
                    <td valign="top">
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>Desconocida</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Miosis</span></p>
                    </td>
                    <td valign="top">
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Edema periorbital</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2" valign="top">
                        <p><span>Trastornos del oído y del laberinto</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>Tinnitus</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Vértigo</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" valign="top">
                        <p><span >Trastornos cardíacos</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>Bradicardia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Palpitaciones</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Taquicardia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="5" valign="top">
                        <p><span>Trastornos vasculares</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>Colapso circulatorio</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Sofocos</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Crisis hipertensiva</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente </span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Hipotensión</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Hipotensión ortostática</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="5" valign="top">
                        <p><span>Trastornos respiratorios, torácicos y mediastínicos</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>Bradipnea</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Broncoespasmo</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Disnea</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Depresión respiratoria </span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Hipo</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Desconocida</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan=17 valign="top">
                        <p><span>Trastornos gastrointestinales</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>Malestar abdominal</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Distensión abdominal</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Dolor abdominal</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Estreñimiento</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Diarrea</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Sequedad de boca</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Dispepsia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Flatulencias</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Gastritis</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Irritación del tracto gastrointestinal </span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Náuseas</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Pancreatitis</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Úlcera péptica con hemorragia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Úlcera péptica con  perforación</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Úlcera péptica</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Arcadas </span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente </span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Vómitos </span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" valign="top">
                        <p><span>Trastornos hepatobiliares</span></p>
                        <p align="center"><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>Hepatitis</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Daño hepatocelular</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Aumento de
                                enzima hepática, incluyendo función hepática alterada y un aumento de la
                                Gamma-glutamiltransferasa</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="9" valign="top">
                        <p><span>Trastornos de la piel y del tejido subcutáneo</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>Acné</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Edema facial</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Hiperhidrosis</span></p>
                    </td>
                    <td valign="top"'>
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Reacciones de fotosensibilidad</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Prurito</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Rash</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Síndrome de Stevens Johnson</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Necrolisis epidérmica tóxica  (Síndrome de Lyell)</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Urticaria</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2" valign="top">
                        <p><span>Trastornos musculoesqueléticos y del tejido conjuntivo</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>Dolor lumbar</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Debilidad</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="8" valign="top">
                        <p><span>Trastornos renales y urinarios</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>Disuria</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Hematuria</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Trastornos en la micción</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Nefritis</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Síndrome nefrótico</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Muy rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Poliuria</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Insuficiencia renal aguda</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Retención urinaria</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2" valign="top">
                        <p><span>Trastornos del aparato reproductor y de la mama</span></p>
                        <p><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p align="left"><span>Alteraciones menstruales</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Alteraciones prostáticas</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="9" valign="top">
                        <p><span>Trastornos generales y alteraciones en el lugar de administración</span></p>
                        <p align="left"><b><span>&nbsp;</span></b></p>
                    </td>
                    <td valign="top">
                        <p><span>Astenia</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Escalofríos</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Malestar general</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Sensación anormal</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p align="left"><span>Síndrome de abstinencia (agitación, ansiedad, nerviosismo,
                                insomnio, hiperquinesia, temblor y síntomas gastrointestinales, síntomas:
                                raros;  ataques de pánico, ansiedad grave, alucinaciones, parestesias,
                                tinnitus, y síntomas inusuales del SNC-p. ej., confusión, delirio,</span></p>
                        <p align="left"><span>despersonalización, pérdida del sentido la realidad, paranoia)</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara/muy rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Fatiga</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Frecuente</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Malestar</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Edema periférico</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Dolor</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" valign="top">
                        <p align="center"><span>Exploraciones </span></p>
                    </td>
                    <td valign="top">
                        <p><span>Aumento de la presión arterial</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Rara</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Aumento de la fosfatasa alcalina en sangre</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p><span>Aumento de la lactato deshidrogenasa en sangre</span></p>
                    </td>
                    <td valign="top">
                        <p><span>Poco frecuente</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                    <td valign="top">
                        <p><span>&nbsp;</span></p>
                    </td>
                </tr>
            </table>

                    
                    
                    
                    
            <p>&nbsp;</p>

            <p><i><u><span>Dexketoprofeno-tramadol</span></u></i></p>

            

            <p><u><span>Comprimidos recubiertos con película: </span></u></p>

            <p><span style='
                                                 '>Las reacciones adversas que se observaron
                    con mayor frecuencia en los estudios clínicos fueron vómitos, náuseas y mareo
                    (2,9%, 2,7% y 1,1% de los pacientes, respectivamente). </span></p>

            <p><span style='
                                                 '>&nbsp;</span></p>

            <p><u><span style='
                                                    '>Granulado para solución oral en sobre:</span></u></p>

            <p><span style='
                                                 '>Las reacciones adversas que se observaron
                    con mayor frecuencia en los estudios clínicos fueron náuseas, somnolencia,
                    vómitos y mareo (3,8%, 3,6%, 3,0% y 2,8% de los pacientes, respectivamente). </span></p>

            <p>&nbsp;</p>

            <p><i><u><span>Dexketoprofeno</span></u></i></p>

            <p><span
                    >Gastrointestinal:
                    Las reacciones adversas que se observan con mayor frecuencia son de naturaleza
                    gastrointestinal. Pueden aparecer úlceras pépticas, perforaciones o hemorragias
                    gastrointestinales, algunas veces mortales, especialmente en pacientes de edad
                    avanzada (ver sección 4.4). Tras la administración, se han notificado casos de
                    náusea, vómitos, diarrea, flatulencia, estreñimiento, dispepsia, dolor
                    abdominal, melenas, hematemesis, estomatitis ulcerativa, exacerbación de
                    colitis y enfermedad de Crohn (ver sección 4.4 Advertencias y precauciones
                    especiales de empleo). Con menor frecuencia, también se ha observado gastritis.
                    En asociación con otros AINE se han notificado casos de edema, hipertensión y
                    fallo cardíaco. </span></p>

           
            <p><span
                    >Como con todos los
                    AINE, podrían presentarse las siguientes reacciones adversas: meningitis
                    aséptica, la cual podría ocurrir predominantemente en pacientes con lupus
                    eritematoso sistémico o enfermedad mixta del tejido conectivo; y reacciones
                    hematológicas (púrpura, anemia aplásica y hemolítica y raramente
                    agranulocitosis e hipoplasia medular).</span></p>

   

            <p><span>Reacciones ampollosas incluyendo Síndrome de Stevens Johnson y Necrolisis Epidérmica
                    Tóxica (muy raros).</span></p>

            

            <p><span
                    >Datos procedentes
                    de ensayos clínicos y de estudios epidemiológicos sugieren que el empleo de
                    algunos AINE (especialmente en dosis altas y en tratamientos de larga duración)
                    puede asociarse con un pequeño aumento del riesgo de acontecimientos
                    aterotrombóticos (p. ej. infarto de miocardio o ictus)(ver sección 4.4).</span></p>

            <p>&nbsp;</p>




            <p><i><u><span>Tramadol</span></u></i></p>

            <p><span
                    >Las reacciones
                    adversas notificadas más frecuentemente debidas a tramadol son náuseas y
                    mareos, ambas ocurren en más del 10% de los pacientes.</span></p>

            

            <p><span>Puede aparecer depresión respiratoria si
                    se excede considerablemente la dosis recomendada y si se administra con otras
                    sustancias depresoras del sistema nervioso central (ver sección 4.5). Se han
                    notificado casos de empeoramiento del asma, aunque no se ha establecido ninguna
                    relación causal. </span></p>

           

            <p><span>Se han observado convulsiones
                    epileptiformes principalmente tras la administración de altas dosis de tramadol
                    o tras el tratamiento concomitante con medicamentos que pueden reducir el
                    umbral convulsivo o que por sí mismos inducen convulsiones cerebrales (ver
                    sección 4.4 y sección 4.5). </span></p>

           

            <p><span>Pueden presentarse síntomas de reacciones
                    de abstinencia similares a los que ocurren durante la abstinencia a opiáceos:
                    agitación, ansiedad, nerviosismo, insomnio, hiperquinesia, temblor y síntomas
                    gastrointestinales. </span></p>

         <p>&nbsp;</p>

            <p><span
                    >Otros síntomas
                    observados muy raramente tras la interrupción del tratamiento con tramadol son:
                    ataques de pánico, ansiedad severa, alucinaciones, parestesias, tinnitus y
                    síntomas inusuales del sistema nervioso central (Ej.: confusión, delirio,
                    despersonalización, pérdida del sentido de la realidad, paranoia).</span></p>

            <p>&nbsp;</p>

            <p><u><span
                        >Notificación de
                        sospechas de reacciones adversas</span></u></p>

            <p><span style='
                                                 '>Es importante notificar sospechas de
                    reacciones adversas al medicamento tras su autorización. Ello permite una
                    supervisión continuada de la relación beneficio/riesgo del medicamento. Se
                    invita a los </span><span>profesionales
                    sanitarios a notificar las sospechas de reacciones adversas a través del
                    Sistema Español de Farmacovigilancia de medicamentos de Uso Humano: </span><a
                    href="https://www.notificaram.es" target="_blank"><span>https://www.notificaram.es</span></a><span>. </span></p>

            <p>&nbsp;</p>
            
            <p class="ft_title_2">4.9 Sobredosis </p>

            <p><span
                    >No se han
                    notificado casos de sobredosis en los estudios clínicos. Se deben considerar
                    los datos notificados para dexketoprofeno y tramadol como principios activos
                    independientes.</span></p>

            <p>&nbsp;</p>
            
            <p><u><span>Síntomas</span></u></p>

            <p><i><span>Dexketoprofeno</span></i></p>

            <p><span>No se conoce la
                    sintomatología después de una sobredosis de dexketoprofeno. Se han producido
                    alteraciones gastrointestinales (vómitos, anorexia, dolor abdominal) y
                    neurológicas (somnolencia, vértigo, desorientación y dolor de cabeza) con
                    medicamentos que contienen dexketoprofeno.</span></p>

        <p>&nbsp;</p>

            <p><i><span
                        >Tramadol</span></i></p>

            <p><span
                    >En caso de
                    sobredosis de tramadol, en principio, aparecen los mismos síntomas que con
                    otros analgésicos que actúan a nivel central (opiáceos). En particular, este
                    cuadro incluye miosis, vómito, colapso cardiovascular, perturbación de la
                    conciencia hasta estados comatosos, convulsiones y depresión respiratoria o
                    incluso paro respiratorio. Se ha notificado también síndrome serotoninérgico.</span></p>

           <p>&nbsp;</p>
           
            <p><u><span>Tratamiento</span></u></p>

            <p><i><span>Dexketoprofeno</span></i></p>

            <p><span>En caso de
                    sobredosis o ingestión accidental, debe iniciarse inmediatamente un tratamiento
                    sintomático en base a la condición clínica del paciente. Si un adulto o un niño
                    hubiesen ingerido más de 5 mg/kg de dexketoprofeno, debería administrarse
                    carbón activo en la primera hora posterior a la ingesta. El dexketoprofeno
                    puede eliminarse mediante diálisis.</span></p>

            <p>&nbsp;</p>

            <p><i><span>Tramadol</span></i></p>

            <p><span
                    >Mantener
                    despejadas las vías respiratorias (y evitar la aspiración), mantener la
                    respiración y circulación según el cuadro sintomatológico. El antídoto en caso
                    de depresión respiratoria es la naloxona. En experimentación animal, naloxona
                    no tiene efecto sobre las convulsiones. En estos casos debe administrarse
                    diazepam por vía intravenosa.</span></p>

            <p>&nbsp;</p>

            <p><span
                    >En caso de
                    intoxicación oral, se recomienda realizar una descontaminación gastrointestinal
                    con carbón activo dentro de las dos horas posteriores a la ingesta de tramadol.
                </span></p>

            <p>&nbsp;</p>

            <p><span
                    >Mediante hemodiálisis
                    o hemofiltración se eliminan cantidades mínimas de tramadol sérico. Por lo
                    tanto, la hemodiálisis o la hemofiltración no pueden ser el único tratamiento
                    de la intoxicación aguda causada por tramadol.</span></p>

            <p>&nbsp;</p>

            <p class="ft_title">5. Propiedades farmacológicas </p>

            <p class="ft_title_2">5.1 Propiedades farmacodinámicas </p>

            <p><span>Grupo farmacoterapéutico: Opioides en combinación con analgésicos no opioides</span></p>

            <p><span>Código ATC: <span>N02AJ14</span></span></p>

            <p>&nbsp;</p>
            
            <p class="ft_title_3"><u>Mecanismo de acción</u></p>

            <p><span>Dexketoprofeno es
                    la sal de trometamina del ácido S-(+)-2-(3-benzoilfenil)propiónico, un fármaco
                    analgésico, antiinflamatorio y antipirético perteneciente a la familia de los
                    antiinflamatorios no esteroideos (M01AE).</span></p>

            

            <p><span>El mecanismo de
                    acción de los antiinflamatorios no esteroideos se relaciona con la disminución
                    de la síntesis de prostaglandinas mediante la inhibición de la vía de la
                    ciclooxigenasa. Concretamente, hay una inhibición de la transformación del
                    ácido araquidónico en endoperóxidos cíclicos, las PGG<sub><span
        >2</span></sub> y PGH<sub><span
        >2</span></sub>, que dan lugar a las
                    prostaglandinas PGE<sub><span>1</span></sub>,
                    PGE<sub><span>2</span></sub>, PGF<sub><span
        >2</span></sub></span><sub><span
    >a</span></sub><span
                    > y PGD<sub><span
        >2</span></sub>, así como a la prostaciclina
                    PGI<sub><span>2</span></sub> y a los
                    tromboxanos (TxA<sub><span>2</span></sub> y
                    TxB<sub><span>2</span></sub>). Además, la
                    inhibición de la síntesis de prostaglandinas podría tener efecto sobre otros
                    mediadores de la inflamación como las quininas, ejerciendo una acción indirecta
                    que se sumaría a su acción directa.</span></p>

           

            <p><span
                    >Se ha demostrado
                    en animales de experimentación y en humanos que el dexketoprofeno es un
                    inhibidor de las actividades COX-1 y COX-2.</span></p>

          

            <p><span
                    >Tramadol
                    hidrocloruro es un analgésico opiaceo sintético de acción central. Es un
                    agonista parcial, no selectivo sobre los receptores opioides μ-,δ- y ĸ- con
                    mayor afinidad por los receptores μ. Su actividad como opiáceo es debida a una
                    baja afinidad de unión de los receptores opiaceos μ- con  el compuesto de
                    origen y de una alta afinidad de unión de éstos con el metabolito O-desmetilado
                    M1. En modelos animales, M1 es hasta 6 veces más potente como analgésico que
                    tramadol y 200 veces más potente en la afinidad de unión a receptores μ. En
                    varios estudios en animales, únicamente se ha antagonizado parcialmente la
                    analgesia inducida por tramadol con el opiáceo antagonista naloxona. La
                    contribución relativa a la analgesia humana de ambos compuestos, tramadol y M1,
                    depende de las concentraciones plasmáticas de cada compuesto. </span></p>


            <p><span
                    >Como otros
                    opiáceos analgésicos, se ha demostrado que tramadol inhibe la recaptación de
                    norepinefrina y serotonina in vitro. Estos mecanismos pueden contribuir de
                    manera independiente al perfil analgésico general de tramadol.  </span></p>

        
            <p><span
                    >Tramadol tiene un
                    efecto antitusivo. En contraposición con morfina, durante un amplio intervalo,
                    dosis analgésicas de tramadol no tienen efecto depresor respiratorio. Además,
                    se producen menos alteraciones de la motilidad gastrointestinal. Sus efectos
                    sobre el sistema cardiovascular tienden a ser leves. La potencia de tramadol es
                    de 1/10 (uno de diez) a 1/6 (uno de seis) respecto a morfina.</span></p>

                    <p>&nbsp;</p>
                    
            <p class="ft_title_3"><u>Efectos farmacodinámicos</u></p>

            <p><span>Estudios
                    preclínicos han demostrado una interacción sinérgica entre los principios
                    activos durante ambos modelos de inflamación, agudo y crónico, y sugieren que
                    dosis más bajas de cada principio activo permiten obtener efecto analgésico. </span></p>

                    <p>&nbsp;</p>
                    
                    <p class="ft_title_3"><u>Eficacia clínica y seguridad</u></p>

            <p><span
                    >Estudios clínicos
                    realizados en diferentes modelos de dolor nociceptivo de moderado a intenso
                    (incluyendo dolor dental, dolor somático o dolor visceral), demostraron una actividad
                    analgésica eficaz de Enanplus. </span></p>

          

            <p><span
                    >En un estudio de
                    dosis múltiple, doble ciego, aleatorizado y de grupos paralelos, realizado en
                    606 pacientes con dolor de moderado a intenso tras una histerectomía abdominal,
                    con una edad media de 47,6 años (rango de 25 a 73), se evaluó la eficacia
                    analgésica de la combinación frente a los componentes de forma individual
                    mediante la suma de los valores de la diferencia de intensidad del dolor
                    durante un intervalo de 8 horas (SPID8) después de la primera dosis de medicación
                    del estudio, siendo evaluada la intensidad del dolor en una escala visual
                    analógica de 100 mm (VAS). Un mayor valor de SPID indica un mayor alivio del
                    dolor. El tramiento con Enanplus resultó en un efecto analgésico
                    significativamente mayor que el de los componentes individuales administrados a
                    la misma dosis (dexketoprofeno 25 mg) o a una dosis más alta (100 mg tramadol),
                    obteniéndose los siguientes resultados: Enanplus (241,8), dexketoprofeno 25 mg
                    (184,5), tramadol 100 mg (157,3). </span></p>

          
            <p><span
                    >A las primeras 8
                    horas tras la administración de Enanplus, los pacientes notificaron una
                    disminución significativa de la intensidad de dolor (PI-VAS media = 33,6) con
                    una diferencia estadísticamente significativa (p&lt; 0,0001) respecto a
                    dexketoprofeno 25 mg (PI-VAS media = 42.6) y tramadol 100 mg (PI-VAS media =
                    42.9). También se demostró una analgesia superior a las 56 horas tras la
                    administración de dosis repetidas de acuerdo a la pauta posológica en la
                    población ITT (intención de tratar), en la que se excluyeron aquellos pacientes
                    que no recibieron tratamiento activo como primera dosis única, con una
                    diferencia estadísticamente significativa (p&lt; 0,0001) entre Enanplus y
                    dexketoprofeno 25 mg (-8,4) y tramadol 100 mg (-5,5), respectivamente.    </span></p>

            

            <p><span
                    >Los pacientes
                    tratados con Enanplus presentaron una menor necesidad de medicación de rescate
                    para el control del dolor (11,8% de los pacientes en comparación con 21,3% (p=
                    0,0104) y 21,4% (p= 0,0097) bajo tratamiento con dexketoprofeno 25 mg y
                    tramadol 100 mg, respectivamente). Si se tiene en cuenta el impacto del uso de
                    la medicación de rescate, se evidencia el efecto analgésico superior de
                    Enanplus en dosis repetidas a las 56 horas, alcanzando una diferencia en PI-VAS
                    que favorece a Enanplus sobre dexketoprofeno (-11.0) y tramadol (-9.1), con una
                    diferencia estadísticamente significativa de p&lt; 0,0001.  </span></p>


            <p><span
                    >En un estudio de
                    dosis múltiple, doble ciego, aleatorizado, de grupos paralelos, realizado en
                    641 pacientes con dolor de moderado a intenso tras una artroplasia total de
                    cadera, con una edad media de 61,9 años (rango de 29 a 80), se evaluó la
                    eficacia analgésica de la combinación frente los componentes individuales a las
                    8 horas después de la primera dosis de medicación del estudio (SPID8). El
                    tratamiento con Enanplus resultó en un efecto analgésico significativamente
                    superior que el de los componentes individuales administrados a la misma dosis
                    (dexketoprofeno 25 mg) o a una dosis más alta (100 mg tramadol); Enanplus
                    (246,9), dexketoprofeno 25 mg (208,8), tramadol 100 mg (204,6). A las primeras
                    8 horas después de la administración de Enanplus, los pacientes notifcaron una
                    disminución significativa de la intensidad del dolor (PI-VAS media= 26,3) con
                    una diferencia estadísticamente significativa (p &lt;0,0001), respecto a
                    dexketoprofeno 25 mg (PI-VAS media = 33,6) y tramadol 100 mg (PI-VAS media =
                    33,7). </span></p>

           

            <p><span
                    >También se
                    demostró una analgesia superior a las 56 horas tras la administración de dosis
                    repetidas de acuerdo a la pauta posológica en la población ITT, en la que se
                    excluyeron aquellos pacientes que no recibieron tratamiento activo como primera
                    dosis única, con una diferencia estadísticamente significativa (p &lt;0,0001)
                    entre Enanplus y dexketoprofeno
                    25 mg (-8,1) y tramadol 100 mg (-6,3), respectivamente.</span></p>

           

            <p><span
                    >La medicación de
                    rescate para controlar el dolor fue requerida por 15,5% de los pacientes
                    tratados con Enanplus, en comparación con un 28,0% (p = 0,0017) y un 25,2% (p =
                    0,0125) de los pacientes tratados con dexketoprofeno 25 mg y tramadol 100 mg,
                    respectivamente. Si se tiene en cuenta el impacto del uso de la medicación de
                    rescate, se evidencia el efecto analgésico superior de Enanplus en dosis
                    repetidas a las 56 horas, alcanzando una diferencia en PI-VAS que favorece a
                    Enanplus sobre dexketoprofeno (-10.4) y tramadol (-8.3), con una diferencia
                    estadísticamente significativa de p&lt; 0,0001.  </span></p>

                    <p>&nbsp;</p>
                    
                    <p class="ft_title_3"><u>Población pediátrica</u></p>

            <p><span>La
                    Agencia Europea del Medicamento ha eximido de la obligación de presentar los
                    resultados de los estudios realizados con Enanplus en todos los subgrupos de la
                    población pediátrica en el tratamiento del dolor agudo moderado o grave (ver
                    sección 4.2 para información sobre el uso pediátrico).</span></p>

                    <p>&nbsp;</p>
                    
            <p class="ft_title_2">5.2 Propiedades farmacocinéticas </p>

            

            <p><span>La administración concomitante de dexketoprofeno y tramadol no tiene efectos en
                    los parámetros farmacocinéticos de cualquiera de los dos componentes en sujetos
                    sanos. </span></p>

           <p>&nbsp;</p>

            <p><u><span>Comprimidos recubiertos con película:</span></u></p>

            
            <p><span>En adultos sanos las concentraciones plasmáticas máximas de dexketoprofeno y tramadol se
                    alcanzan en aproximadamente 30 minutos (rango de 15 a 60 minutos) y  en 1,6 a 2
                    horas, respectivamente.  </span></p>

            <p>&nbsp;</p>

            <p><u><span
    >Granulado para solución oral en sobre:  </span></u></p>

            <p><span>Se realizó un
                    estudio de bioequivalencia en voluntarios sanos para comparar Enanplus 75 mg /
                    25 mg de granulado para solución oral en sobre frente a los comprimidos
                    recubiertos con película. Para dexketoprofeno, las dos formulaciones fueron
                    bioequivalentes en términos de grado de biodisponibilidad (AUC); las
                    concentraciones máximas (Cmax) fueron aproximadamente un 15% mayores después
                    del granulado para solución oral en sobre en comparación con el comprimido
                    recbuierto con película. Respecto a tramadol, las dos formulaciones fueron
                    bioequivalentes en términos de velocidad y grado de absorción.</span></p>

            

            <p><u>Dexketoprofeno</u></p>

            <p><i><u><span>Absorción</span></u></i></p>

            <p><u><span>Comprimidos recubiertos con película: </span></u></p>

            <p><span>Después
                    de la administración oral de dexketoprofeno en humanos, la Cmax se alcanza a
                    los 30 minutos (rango de 15 a 60 minutos). </span></p>

            <p>&nbsp;</p>

            <p><u><span>Granulado para solución oral en sobre: </span></u></p>

            <p><span
                    >Dexketoprofeno se
                    absorbe rápidamente tras su administración oral. Cuando se administra en forma
                    de Enanplus 75 mg/ 25 mg granulado para solución oral, se alcanzan
                    concentraciones plasmáticas detecables a partir de 5 minutos (848,5 ng/ml, SD =
                    459,51 ng/ml) y se alcanza la C<sub>max</sub> (3192,0 ng/mL) después de 17
                    minutos (rango de 15 – 50 minutos). </span></p>

          

            <p><span>Cuando se
                    administra conjuntamente con alimentos, el AUC no se modifica, sin embargo la 
                    C<span>max</span> del dexketoprofeno se
                    reduce y su velocidad de absorción se retrasa (incremento de t<span
    >max</span>).</span></p>

    
    <p>&nbsp;</p>
    
            <p><i><u><span>Distribución</span></u></i></p>

            <p><span
                    >Los valores de la
                    semivida de distribución y de eliminación de dexketoprofeno son 0,35 y 1,65
                    horas, respectivamente. Al igual que otros fármacos con elevada unión a
                    proteínas plasmáticas (99 %), su volumen de distribución tiene un valor medio
                    inferior a 0,25 l/kg. En los estudios farmacocinéticos realizados a dosis
                    múltiple, se observó que el AUC tras la última administración no difiere de la
                    obtenida a dosis única, indicando por lo tanto que no se produce acumulación
                    del fármaco.</span></p>

    <p>&nbsp;</p>

            <p><i><u><span>Biotransformación y eliminación</span></u></i></p>

            <p><span
                    >Tras la
                    administración de dexketoprofeno, en orina sólo se obtiene el enantiómero S(+),
                    demostrando que no se produce conversión al enantiómero R-(-) en humanos. </span></p>

            <p><span
                    >La principal vía
                    de eliminación para el dexketoprofeno es la glucuronoconjugación seguida de
                    excreción renal.</span></p>

            <p>&nbsp;</p>

            <p><u><span>Tramadol</span></u></p>

            <p class="ft_title_3"><u>Absorción</u></p>

            <p><span
                    >Tras la
                    administración oral, se absorbe más del 90% de tramadol. El valor medio de
                    biodisponibilidad absoluta es de aproximadamente el 70%, independientemente de
                    la ingesta concomitante de comida. </span></p>

            <p><span
                    >La diferencia
                    entre el tramadol absorbido y no metabolizado disponible es probablemente
                    debido a un bajo efecto de primer paso.  El efecto de primer paso máximo tras
                    la administración oral es de un 30%. </span></p>

            

            <p><span
                    >Tramadol tiene una
                    alta afinidad tisular (Vd,β= 203±40l). La unión a proteínas plasmáticas es de
                    aproximadamente un 20%. </span></p>

            <p>&nbsp;</p>

            <p><u><span
                        >Comprimidos
                        recubiertos con película:</span></u></p>

            <p><span
                    >Después de la
                    administración de una dosis oral única de tramadol 100 mg en forma de cápsulas
                    o comprimidos a voluntarios jóvenes sanos, las concentraciones plasmáticas
                    fueron detectables dentro de aproximadamente 15 a 45 minutos, con un valor
                    medio de Cmax de 280 a  208 mcg/L y de Tmax de 1,6 a 2 horas.  </span></p>

            <p>&nbsp;</p>

            <p><u><span
                        >Granuado para
                        solución oral en sobre: </span></u></p>

            <p><span>Tramadol
                    se administra como un racémico y se detectan ambos enantiómeros [+] y [-] en la
                    circulación. </span></p>

            <p><span>Las
                    concentraciones plasmáticas máximas de los enantiómeros de tramadol [+] y [-]
                    cuando se administra en forma Enanplus 75 mg / 25 mg granulado para solución
                    oral en sobre, son 158,9 y 142,0 ng /ml respectivamente, y se alcanzan en 38
                    minutos (rango 15 minutos - 2 horas).</span></p>

            <p>&nbsp;</p>

            <p class="ft_title_3"><u>Distribución</u></p>

            <p><span>Tramadol
                    atraviesa la barrera hematoencefálica y la barrera placentaria. Se encuentran
                    cantidades muy pequeñas del principio activo y de su derivado O-desmetilado en
                    la leche materna (0,1 % y 0,02 % respectivamente de la dosis administrada).</span></p>

                    <p>&nbsp;</p>
                    <p class="ft_title_3"><u>Biotransformación</u></p>

            <p><span
                    >La metabolización
                    de tramadol en humanos tiene lugar principalmente mediante N-desmetilación y
                    O-desmetilación así como por la conjugación de los derivados O-desmetilados con
                    ácido glucurónico. Únicamente O-desmetiltramadol es farmacológicamente activo.
                    Existen considerables diferencias cuantitativas interindividuales entre los
                    demás metabolitos. Hasta ahora se han identificado 11 metabolitos en orina. Los
                    estudios realizados en animales han demostrado que O-desmetiltramadol es de 2-4
                    veces más potente que la sustancia de origen. La vida media t1/2ß (6
                    voluntarios sanos) es de 7,9 h (intervalos 5,4-9,6 h) y es aproximadamente la
                    de tramadol.</span></p>

            <p><span
                    >La inhibición de
                    uno o de ambos tipos de isoenzimas del citocromo P450, CYP3A4 y CYP2D6,
                    implicados en la biotransformación de tramadol, puede afectar a la concentración
                    plasmática de tramadol o de su metabolito activo. </span></p>

                   <p>&nbsp;</p> 
                    
                    
                    <p class="ft_title_3"><u>Eliminación</u></p>

            <p><span
                    >Independientemente
                    del modo de administración, la vida media de eliminación t<sub>1/2ß </sub>es
                    aproximadamente 6h. En pacientes mayores de 75 años, este valor puede aumentar
                    aproximadamente 1,4 veces.</span></p>

            

            <p><span>Tramadol y sus metabolitos se eliminan casi completamente por vía renal. La eliminación urinaria acumulada es del
                    90% de la radioactividad total de la dosis administrada. 
                    En caso de insuficiencia renal y hepática la vida media puede estar ligeramente prolongada. En pacientes
                    con cirrosis hepática, la vida media de eliminación es de 13,3±4,9 h (tramadol)
                    y 18,5±9,4 h (O-desmetiltramadol), y en un caso extremo se determinaron 22,3 h
                    y 36 h respectivamente. En pacientes con insuficiencia renal (aclaramiento de
                    creatinina &lt; 5 ml/min) los valores fueron 11±3,2 h y 16,9±3 , y en un caso
                    extremo fueron 19,5 h y 43,2 h respectivamente.</span></p>
<p>&nbsp;</p>


<p class="ft_title_3"><u>Linealidad/ No linealidad</u></p>

            <p><span>El
                    perfil farmacocinético de tramadol es lineal dentro del margen de dosificación
                    terapéutico. La relación entre concentraciones séricas y el efecto analgésico
                    es dosis-dependiente, sin embargo puede variar considerablemente en casos
                    aislados. En general, es eficaz una concentración sérica de 100 - 300 ng/ml.</span></p>

                    <p>&nbsp;</p>
                    
            <p class="ft_title_2">5.3 Datos preclínicos sobre seguridad </p>

            <p><u><span>Combinación tramadol hidrocloruro-dexketoprofeno</span></u></p>

            <p><span>Según los estudios convencionales de
                    seguridad farmacológica y toxicidad de dosis repetidas, los datos preclínicos
                    con la combinación no muestran riesgos especiales para humanos.  </span></p>

            

            <p><span>La combinación de dexketoprofeno y
                    tramadol no tuvo efecto significativo sobre el sistema cardiovascular según lo
                    evaluado en los test <i>in vivo</i> e <i>in vitro</i>. Se observó un efecto
                    sobre el tránsito gastrointestinal menor con la combinación en comparación con
                    tramadol solo. </span></p>

      

            <p><span style='
                                                 '>En un estudio de toxicidad crónica en
                    ratas de 13 semanas, se observó un NOAEL (No Observed Adverse Effect Levels) de
                    6 mg/kg/día para dexketoprofeno y 36 mg/kg/día para tramadol (a las dosis más
                    altas probadas), cuando se administran tanto individualmente como en
                    combinación (correspondiendo a exposiciones basadas en AUC a las dosis únicas
                    del NOAEL de 25.10 veces y 1.38 veces la exposición humana a dexketoprofeno y
                    tramadol, respectivamente, a una única dosis clínica de 25 mg de dexketoprofeno
                    y 75 mg de tramadol). </span></p>

            <p><span style='
                                                 '>No se observaron nuevas toxicidades a
                    diferencia de lo anteriormente descrito para dexketoprofeno o tramadol.</span></p>

            <p>&nbsp;</p>

            <p><u><span>Dexketoprofeno</span></u></p>

            <p><span>Los estudios convencionales de seguridad
                    farmacológica, toxicidad de dosis repetidas, genotoxicidad, toxicidad en la reproducción
                    e inmunofarmacología, no mostraron riesgos especiales en humanos. Los estudios
                    de toxicidad crónica realizados en ratones y monos dieron un NOAEL (No Observed
                    Adverse Effect Levels) de 3 mg/kg/día. A dosis altas, el efecto adverso más
                    observado fue erosiones gastrointestinales y úlceras que se desarrollaron de
                    manera dosis-dependiente. </span></p>

            <p>&nbsp;</p>
            
            <p><u><span>Tramadol</span></u></p>

            <p><span style='
                                                 '>En exploraciones hematológicas,
                    clínico-químicas e histológicas de la administración oral y parenteral repetida
                    de tramadol en ratas y perros durante 6 a 26 semanas y la administración oral
                    en perros durante 12 meses, no mostraron evidencias de ningún cambio
                    relacionado con las sustancias. Sólo se produjeron manifestaciones del sistema
                    nervioso central tras dosis altas, considerablemente por encima del rango
                    terapéutico: inquietud, salivación, convulsiones y reducción de ganancia de
                    peso. Ratas y perros toleraron dosis orales de 20 mg/kg y 10 mg/kg del peso
                    corporal, respectivamente. Los  perros toleraron dosis rectales de 20 mg/kg del
                    peso corporal sin ninguna reacción.  </span></p>

           

            <p><span style='
                                                 '>En ratas, dosis de tramadol de 50
                    mg/kg/día en adelante causaron efectos tóxicos en las madres y aumentó la
                    mortalidad neonatal. El retraso en la descendencia se produjo en forma de
                    trastornos de osificación y retraso en la abertura vaginal y los ojos. La
                    fertilidad masculina no se vió afectada. Después de dosis más altas (de 50
                    mg/kg/día en adelante) las hembras mostraron una tasa de embarazo reducida. En
                    conejos hubo efectos tóxicos en las madres desde más de 125 mg/kg en adelante y
                    anomalías esqueléticas en la descendencia.</span></p>

          

            <p><span>En algunos sistemas de estudio in vitro no
                    hubo evidencia de efectos mutagénicos. Los estudios in vivo no mostraron tales
                    efectos.</span></p>

            

            <p><span style='
                                                 '>De acuerdo con los conocimientos
                    adquiridos hasta el momento, tramadol puede ser clasificado como no mutagénico.</span></p>

           

            <p><span style='
                                                 '>Se han realizado estudios en ratas y
                    ratones sobre el potencial tumorigénico de tramadol hidrocloruro. El estudio en
                    ratas no mostró evidencia de aumento en la incidencia de tumores relacionados
                    con las sustancias. En el estudio en ratones se produjo un aumento de la
                    incidencia de adenomas de células hepáticas en animales machos (aumento
                    dosis-dependiente, no significativo desde 15 mg/kg en adelante) y un aumento en
                    los tumores pulmonares en las hembras de todos los grupos de dosis
                    (significativo, pero no dosis-dependiente).</span></p>

            <p>&nbsp;</p>

            <p class="ft_title">6. Datos farmacéuticos </p>

            <p class="ft_title_2">6.1 Lista de excipientes </p>

            <p><u><span>Comprimidos
                        recubiertos con película: </span></u></p>

            <p><i><span>Núcleo
                        del comprimido</span></i><span>:</span></p>

            <p><span>Celulosa
                    microcristalina;</span></p>

            <p><span>Almidón
                    de maíz pregelatinizado;</span></p>

            <p><span>Croscarmelosa
                    sódica;</span></p>

            <p><span>Estearil
                    fumarato sódico;</span></p>

            <p><span>Sílice
                    coloidal anhidra.</span></p>

            <p>&nbsp;</p>

            <p><i>Recubrimiento:</i></p>

            <p>Opadry II blanco 85F18422 compuesto por: </p>

            <p>Alcohol polivinilico;</p>

            <p>Dioxido de titanio;</p>

            <p>Macrogol/PEG 3350;</p>

            <p>Talco.</p>

            <p>&nbsp;</p>

            <p><u><span>Granulado
                        para solución oral en sobre:</span></u></p>

            <p><span>Sacarosa</span></p>

            <p><span>Aroma
                    de limón</span></p>

            <p><span>Acesulfame
                    potásico (E-950)</span></p>

            <p>&nbsp;</p>

            <p class="ft_title_2">6.2 Incompatibilidades </p>

            <p><span>No procede. </span></p>
            
            <p>&nbsp;</p>
            

            <p class="ft_title_2">6.3 Periodo de validez </p>

            <p><u><span>Comprimidos
                        recubiertos con película: </span></u></p>

            <p 
><span 
>- 30 meses si el producto está envasado en
                    PA/Aluminio/PVC//Blíster de aluminio y PVC/PVDC/Blíster de aluminio; </span></p>

            <p 
><span 
>- 24 meses si el producto está envasado en PVC/PE/PVDC/Blíster de
                    aluminio. </span></p>

            <p>&nbsp;</p>

            <p><u><span>Granulado
                        para solución oral en sobre:</span></u></p>

            <p 
><span 
>3 años.</span></p>

            <p>&nbsp;</p>

            <p class="ft_title_2">6.4 Precauciones especiales de conservación </p>

            <p><span>Este
                    medicamento no requiere ninguna temperatura especial de conservación. </span></p>

          

            <p><span>Conservar
                    en el envase original para protegerlo de la luz. </span></p>

            <p>&nbsp;</p>

            <p class="ft_title_2">6.5 Naturaleza y contenido del envase </p>

            <p><u><span 
     >Comprimidos recubiertos con película: </span></u></p>

            <p><span 
  >Los comprimidos recubiertos con película se presentan en blísters,
                    en tres materiales alternativos:</span></p>

            <p  style='
'><span
>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span 
                >PA/Aluminio/PVC//Blíster de aluminio</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span 
                >PVC/PE/PVDC//Blíster de aluminio</span></p>

            <p><span>-<span
    >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span 
                >PVC/PVDC//Blíster de aluminio  </span></p>

            <p><span>Enanplus<span
    >: envases de 2, 4, 10, 15, 20, 30, 50, 100 comprimidos
                        recubiertos con película o multienvases que contienen 500 comprimidos
                        recubiertos con película por envase (5 envases de 100 comprimidos).</span></span></p>

            <p>&nbsp;</p>

            <p><u><span>Granulado
                        para solución oral en sobre:</span></u></p>

            <p><span>El
                    granulado para solución oral se presenta en sobres formados por una lámina
                    multicapa termosellada de papel-aluminio-polietileno (como copolímero con
                    acetato de vinilo) en una caja de cartón. </span></p>

     

            <p><span><span>Envases de 2, 3, 10,
                        15, 20, 50, 100 y 500 sobres. </span></span></p>

         

            <p><span 
  >Puede que solamente estén comercializados algunos tamaños de
                    envases.  </span></p>

                    <p>&nbsp;</p>
                    
            <p class="ft_title_2">6.6 Precauciones especiales de eliminación y otras
                manipulaciones </p>

            <p><span>Ninguna
                    especial. </span></p>

            <p><span>La
                    eliminación del medicamento no utilizado y de todos los materiales que hayan
                    estado en contacto con él, se realizará de acuerdo con la normativa local.</span><span
                    > </span></p>

            <p>&nbsp;</p>

            <p class="ft_title">7. Titular de la autorización de comercialización </p>

            <p><span>Laboratorios
                    Menarini, S.A.</span></p>

            <p><span>Alfons
                    XII, 587</span></p>

            <p><span>E
                    08918 Badalona (Barcelona) </span></p>

            <p><span>España</span></p>

            <p>&nbsp;</p>

            <p class="ft_title">8. Número(s) de autorización de comercialización </p>

            <p><u><span 
     >Comprimidos recubiertos con película: </span></u></p>

            <p><span>80.925</span></p>

            <p>&nbsp;</p>

            <p><u><span>Granulado
                        para solución oral en sobre:</span></u></p>

            <p><span>83.077</span></p>

            <p>&nbsp;</p>

            <p class="ft_title">9. Fecha de la primera autorización/ renovación de la autorización</p>

            <p><u><span>Comprimidos recubiertos con película: </span></u></p>

            <p><span>Fecha de la primera autorización: Mayo 2016 </span></p>

            <p><span>Fecha de la última renovación: 13.10.2020</span></p>

            <p>&nbsp;</p>

            <p><u><span>Granulado para solución oral en sobre:</span></u></p>

            <p><span>Fecha de la primera autorización: 27.07.2018</span></p>

            <p><span>Fecha de la última renovación: 14.11.2022</span></p>

            <p>&nbsp;</p>

            <p class="ft_title">10. Fecha de la revisión del texto</p>

            <p><span><span>Enanplus 75mg/25mg comprimidos recubiertos con película: Diciembre 2022</span></span></p>

            <p><span><span>Enanplus 75mg/25mg granulado para solución oral en sobre: Diciembre 2022</span></span></p>

                        
            <p>&nbsp;</p>
                        
            <p class="ft_title">11. PRESENTACIÓN Y PVP</p>

            <p><span>Enanplus 75 mg/25 mg comprimidos recubiertos con película:</span></p>

            <p><span>Envase
                    de 20 comprimidos recubiertos con película, PVP<sub>IVA</sub>: 7,10€</span></p>

            <p><span>Envase
                    de 100 comprimidos recubiertos con película, PVP<sub>IVA</sub>: 24,91€</span></p>

            <p>&nbsp;</p>

            <p><span>Enanplus
                    75 mg/25 mg granulado para solución oral en sobre, 20 sobres, PVP<sub>IVA</sub>:
                    7,10€</span></p>

            <p>&nbsp;</p>

            <p><b><span>12. CONDICIONES DE DISPENSACIÓN </span></b></p>

            <p><span>Medicamento sujeto a prescripción médica</span></p>

            <p><span>Financiado por el Sistema Nacional de Salud</span></p>

            <p>&nbsp;</p>

            <p><b><span>FECHA DE ELABORACIÓN DEL DOCUMENTO</span></b><span>: Diciembre 2022</span></p>

        </div>

`;