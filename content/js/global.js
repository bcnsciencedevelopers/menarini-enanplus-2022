/*
 * Configuración de variables del eDetailing
 * 
 * currentSlide es para indicar en que slide tiene que empezar el eDetailing.
 * zoomSlides: true permite hacer zoom en las slides, false no permite hacer zoom.
 * route: es la ruta por donde empieza
 * class_prev: es la clase o id de la flecha de prev
 * class_next: es la clase o id de la flecha de next
 * openPopupClass: son las clases o ids que pondremos en inhabilitados cuando se abre un popup
 * 
 */
// variables que se pueden cambiar
var currentSlide = 0;
var zoomSlides = true;
var route = "default"; 
var class_prev = ".prev";
var class_next = ".next";
var openPopupClass = ".logo_producto, .logo_cliente, .btn_thumbs, .btn_i, .btn_biblio, .next, .prev";

//variables de sistema
var next = "";
var prev = "";
var scroll_thumbs = null;
var timeEvents = {};
var eventToEnd = null;

/*
 * Init fastclick
 */
if ('addEventListener' in document) {
    document.addEventListener('DOMContentLoaded', function () {
        FastClick.attach(document.body);
    }, false);
}

//true lanza los títulos de las slides, false no lanza los títulos, los lanza como eventos
var trackingTitleSlide = false;

$(document).ready(function () {

    // $.getScript("content/js/FT.js", function() {

    $("#global").append(`
        <div class='btn_home' data-slide="1" data-route="default"></div>

        <div class='logo_producto' onclick='openPop("popFT")'></div>
        <div class='logo_cliente'></div>
        <div class='btn_thumbs'></div>
        <div class='btn_i' data-slide="20" data-route="information"></div>
        <div class='btn_biblio' onclick='openPop("popRef")'></div>

        <div class="btn1 btnMenu" data-slide="2" data-route="default"></div>
        <div class="btn2 btnMenu" data-slide="3" data-route="default"></div>
        <div class="btn3 btnMenu" data-slide="4" data-route="default"></div>
        <div class='btn4 btnMenu' data-slide="7" data-route="default"></div>
        
        <div class='btn5 btnMenu' data-slide="17" data-route="default"></div>
        <div class='btn6 btnMenu' data-slide="19" data-route="default"></div>

        <div class='prev' onclick="prevSlide()"></div>
        <div class='next' onclick="nextSlide()"></div>
        
        <div class='popup' id='popRef'>
            <div id='closeRef' class='close' onclick='closePop("popRef")'></div>
            <img id='imgRef' src='content/css/images/ref.png'>
        </div>

        <div class='popup' id='popFT'>
            <div class="search-bar">
                <input type="search">
                <button data-search="next">&darr;</button>
                <button data-search="prev">&uarr;</button>
                <div class="btnSearch"></div>
            </div>
            <div id='closeFT' class='close' onclick='closePop("popFT")'></div>
            <div class='ft-container'>${ft}</div>
        </div>
        <div id="footerBg"></div>
    `);

    // FT
    addFTSearch();
    // END FT

    document.ontouchmove = function (e) {
        e.preventDefault()
    };

    //para cambiar de slide y de ruta
    $('body').on('click','[data-slide]', function () {
        var a_slide = $(this).data('slide');;
        var a_route = $(this).data('route');
        if( a_route!=undefined ){
            irA( a_slide, a_route );
            removeAndCreateThumbs();
        }
        else{
            irA(a_slide);
        }
    });

    initHammer();
    
    crearBarraThumbs();
    
    //arrancamos a ver enque página empieza
    aux_slide = getCurrentPage();
    setTimeout(function(){
        trackPageChange( currentSlide );    
    },500);
    
});

function getCurrentPage(){
    var aux_page = 0;
    
    $('.slide').each(function(i, obj) {
        if( obj.classList.contains('active') ){
            currentSlide = obj.id.replace(/slide-/g, "");
            aux_page = currentSlide;
        }
    });
    
    console.log("currentSlide: "+currentSlide);
    console.log("route: "+route);
    checkPrevNext();
    
    
    //launchBookmarkPage(currentIndex);
    if( aux_page==0 ){
        irA(navegation[route][0]);
    }
    
    
    //lanzamos interaccion por slide 
    slideEspec();
    return currentSlide;
}

/**
 * Cambia de slide y lanza el getCurrentPage
 * @param {type} slide
 * @returns {undefined}
 */
function irA(slide, ruta, element) {
    // alert(slide);
    if (typeof slide == "number") {
        slide = "slide-" + slide;
    }

    $(".slide.active").removeClass("active");
    $("#" + slide).addClass("active");
    
    if (ruta){
        route = ruta;
    }

    camino = $(element).data("camino");

    if (camino) {
        route = camino
    }
    
    aux_slide = getCurrentPage();
    trackPageChange( aux_slide );
    $("#openMenu").removeClass("active");
    $(".overlay").removeClass("active");
}

// checks
// <input onchange="addCheckEvent('att1')" type="checkbox" id="att1" > Attribute 1
function addCheckEvent(checkbox) {
    var val = $('#' + checkbox).prop('checked') + '';//Only string value
    return Tracking.startEvent('product_attribute', 'Product attribute ' + checkbox, val);
}

function addFTSearch() {
    // the input field
    var $input = $("input[type='search']"),
        // clear button
        $clearBtn = $("button[data-search='clear']"),
        // prev button
        $prevBtn = $("button[data-search='prev']"),
        // next button
        $nextBtn = $("button[data-search='next']"),
        // the context where to search
        $content = $("#popFT"),
        // jQuery object to save <mark> elements
        $results,
        // the class that will be appended to the current
        // focused element
        currentClass = "current",
        // top offset for the jump (the search bar)
        offsetTop = 50,
        // the current index of the focused element
        currentIndex = 0;

    /**
     * Jumps to the element matching the currentIndex
     */

    /* nearest | center */

    $.fn.ensureVisible = function () {
        $(this).each(function () {
            $(this)[0].scrollIntoView({behavior: 'smooth', block: 'center'});
        });
    };

    function jumpTo() {
        if ($results.length) {
            var position,
                    $current = $results.eq(currentIndex);
            $results.removeClass(currentClass);
            if ($current.length) {
                $current.addClass(currentClass);
                position = $current.offset().top - offsetTop;
                setTimeout(function () {
                    position_layer = $('.current').position().top;
                    //   var relativeY = $('.current').offset().top - $('.initial_position').offset().top;
                    $('.current').ensureVisible();
                }, 300);
            }
        }
    }

    /**
     * Searches for the entered keyword in the
     * specified context on input
     */
    $input.on("input", function () {
        var searchVal = this.value;
        $content.unmark({
            done: function () {
                $content.mark(searchVal, {
                    separateWordSearch: true,
                    done: function () {
                        $results = $content.find("mark");
                        currentIndex = 0;
                        //jumpTo();
                    }
                });
            }
        });
    });

    /**
     * Clears the search
     */
    $clearBtn.on("click", function () {
        $content.unmark();
        $input.val("").focus();
    });

    /**
     * Next and previous search jump to
     */
    $nextBtn.add($prevBtn).on("click", function () {
        if ($results.length) {
            currentIndex += $(this).is($prevBtn) ? -1 : 1;
            if (currentIndex < 0) {
                currentIndex = $results.length - 1;
            }
            if (currentIndex > $results.length - 1) {
                currentIndex = 0;
            }
            jumpTo();
        }
    });
}

function initHammer() {
    var page = document.body;
    var mc = new Hammer(page);

    var posX = 0,
    posY = 0,
    scale = 1,
    last_scale = 1,
    last_posX = 0,
    last_posY = 0,
    max_pos_x = 0,
    max_pos_y = 0,
    transform = "",
    el = page;

    var aux_eventos = "swipeleft swiperight";
    if (zoomSlides) {//true hacemos zoom
        mc.get("swipe").set({direction: Hammer.DIRECTION_ALL, threshold: 200});
        mc.get('pinch').set({enable: true});
        aux_eventos = "swipeleft swiperight doubletap pan pinch panend pinchend";
    } else { //por defecto no hay zoom
        mc.get("swipe").set({direction: Hammer.DIRECTION_ALL, threshold: 200});
        aux_eventos = "swipeleft swiperight";
    }
    mc.on(aux_eventos, function (ev) {
        
        if (ev.type == "swipeleft" && scale.toFixed(2) == "1.00") {//scale es para que no haga swipe si esta el zoom puesto
            //next
            if (next != "") {
                nextSlide();
            }
        } else if (ev.type == "swiperight" && scale.toFixed(2) == "1.00") {//scale es para que no haga swipe si esta el zoom puesto
            //prev
            if (prev != "") {
                prevSlide();
            }
        }
        
        /*if (ev.type == "doubletap") {
            transform =
                "translate3d(0, 0, 0) " +
                "scale3d(2, 2, 1) ";
            scale = 2;
            last_scale = 2;
            try {
                if (window.getComputedStyle(el, null).getPropertyValue('-webkit-transform').toString() != "matrix(1, 0, 0, 1, 0, 0)") {

                    transform =
                        "translate3d(0, 0, 0) " +
                        "scale3d(1, 1, 1) ";
                    scale = 1;
                    last_scale = 1;
                }
            } catch (err) {}
            el.style.webkitTransform = transform;
            transform = "";
        }*/

        //pan    
        if (scale != 1) {
            posX = last_posX + ev.deltaX;
            posY = last_posY + ev.deltaY;
            max_pos_x = Math.ceil((scale - 1) * el.clientWidth / 2);
            max_pos_y = Math.ceil((scale - 1) * el.clientHeight / 2);
            if (posX > max_pos_x) {
                posX = max_pos_x;
            }
            if (posX < -max_pos_x) {
                posX = -max_pos_x;
            }
            if (posY > max_pos_y) {
                posY = max_pos_y;
            }
            if (posY < -max_pos_y) {
                posY = -max_pos_y;
            }
        }

        //pinch
        if (ev.type == "pinch") {
            scale = Math.max(.999, Math.min(last_scale * (ev.scale), 4));
        }

        if (ev.type == "pinchend") {
            last_scale = scale;
            transform =
                        "translate3d(0, 0, 0) " +
                        "scale3d(1, 1, 1) ";
            scale = 1;
            last_scale = 1;
        }

        //panend
        if (ev.type == "panend") {
            last_posX = posX < max_pos_x ? posX : max_pos_x;
            last_posY = posY < max_pos_y ? posY : max_pos_y;
        }

        if (scale != 1) {
            transform =
                "translate3d(" + posX + "px," + posY + "px, 0) " +
                "scale3d(" + scale + ", " + scale + ", 1)";
        }

        if (transform) {
            el.style.webkitTransform = transform;
            transform = "";
        }
    });
}

/**
 * Carga en prev y next si hay siguiente o no
 * @returns {undefined}
 */
function checkPrevNext(){
    var aux_nav = navegation[route];
    
    //buscamos en que posición del camino estamos
    let index = aux_nav.indexOf( parseInt( currentSlide ) );

    if(index != -1){
        //el prev
        if( index > 0 ){
            prev = navegation[route][index-1];
            $(class_prev).addClass("active");
        }
        else if( index == 0 ){
            prev = "";
            $(class_prev).removeClass("active");
        }
        
        //next
        if( (index+1) < aux_nav.length ){
            // debugger;
            next = navegation[route][index+1];
            $(class_next).addClass("active");
        }
        else if( (index+1) == aux_nav.length ){
            next = "";
            $(class_next).removeClass("active");
        }
    }
    console.log("Next:"+next);
    console.log("Prev:"+prev);
}






function crearBarraThumbs() {
    console.log("crearBarraThumbs");
    
    $("body").append("<div id='openMenu'><div class='menuWrapper'></div></div>");
    $("body").append("<div class='overlay'></div>");

    $(".btn_thumbs").on("click", function (e) {
        e.stopPropagation();
        $("#openMenu").addClass("active");
        $(".overlay").addClass("active");
    });

    $("#openMenu").on("click", function (e) {
        e.stopPropagation();
    });

    $("body").on("click", function () {
        $("#openMenu").removeClass("active");
        $(".overlay").removeClass("active");
    });
    
    scroll_thumbs = new IScroll('#openMenu', {scrollY: true, scrollbars: 'custom'});
    
    removeAndCreateThumbs();    
}


function removeAndCreateThumbs(){
    let str = "";
    let a_nav = navegation[route];
    
    scroll_thumbs.destroy();
    scroll_thumbs = null;
    $(".menuWrapper ").html("");

    for (let i = 0; i < a_nav.length; i++) {
        str += `
            <div class="slide_diapo" onClick="irA(${a_nav[i]})">
                <img src="./thumbnails/${a_nav[i]}.png" class="slide_thumb" />
            </div>`;
    }
    str += "<br><br>";
    
    $(".menuWrapper").append(str);
        
    setTimeout(function () {
        scroll_thumbs = new IScroll('#openMenu', {scrollY: true, scrollbars: 'custom'});
    }, 800);
}



function nextSlide() {
    console.log("next");
    console.log("next: " + next);
    if( next!='' ){
        irA( next, route );
    }
}

function prevSlide() {
    if( prev!='' ){
        irA( prev, route );
    }
}

function openPop(id) {
    $("#" + id).css('display', "block");
    $("#" + id).css('z-index', 10);
    $("#" + id + " *").css('z-index', 10);
    
    $("#" + id).animate({
        opacity: 1
    }, 100, function () {});
    if( id!="popFT" ){
        $(openPopupClass).addClass('inactive');
    }
}

function closePop(id) {

    $("#" + id + " *").css('z-index', -1);
    $("#" + id).animate({
        opacity: 0
    }, 50, function () {
        $("#" + id).css('display', "none");
        $("#" + id).css('z-index', -1);
    });
    
    $(openPopupClass).removeClass('inactive');
}

function lanzaTitleSlideAnalitica( titulo, eventId ){
    if (window.navigator.standalone !== undefined) { //ipad
        setTimeout(function() {
            
            Tracking.startEvent(titulo, "Time", null, true, false).then(function(eventId){
                    slideEventId = eventId
            }, function(err){
                    console.error(err);
            });

            window.addEventListener('beforeunload', (event) => {
                    return Tracking.endEvent(slideEventId).then(function(){
                            slideEventId = null;
                    },function(err){
                            //Error occurred
                            console.error(err);
                    });
            });
        }, 200);
        
    }
}




/* 
* id del evento, en este caso del bookmark, tiene que estar en string
* descripción: bookmark, page, etc
* atributo: textbox, checkbox selected, button, 
* Cron: para contar el tiempo
* test: false o true, se pone de momento false
*/
function launchBookmarkPage( nPage ){
    
    try{
        if (currentTracking != undefined) {
            Tracking.endEvent(currentTracking);
            currentTracking = undefined;
        }
                
        Tracking.startEvent("" + nPage, 'bookmark', 'page', true, false).then(
            function(idEvent){
                currentTracking = idEvent
            },
            function(err){
                console.error(err);
            }
        );
        
        Tracking.endEvent(currentTracking);
        
    }catch(error){
        console.log(error);
    }
    //Tracking.startEvent(nPage,'PAGE',null,true,false);
}


/*
 * Para activar o realizar algo depende de la slide
 * @returns {undefined}
 */
function slideEspec(){   
    switch( currentSlide ){
        case "20" : 
            $(".btn_i").addClass('active');
            break;
        default:
            $(".btn_i").removeClass('active');
            break;
    }
}

function goToPage(page) {
    irA( page );
}

function trackPageChange(currentPage) {
    if (eventToEnd != null) {
        const currentAttributeEventId = getDataEventIdFromPage(eventToEnd);
        endVisitEvent(currentAttributeEventId);
    }
    const activeAttributeEventId = getDataEventIdFromPage(currentPage);
    callVisitEvent(activeAttributeEventId, 'Page');
    eventToEnd = activeAttributeEventId;
}

function trackEventClick(element, eventDescription) {
    const eventId = element.getAttribute('data-md-event-id');
    callVisitEvent(eventId, eventDescription);
}

function callVisitEvent(eventId, eventDescription) {
    visitEvent(eventId, eventDescription, null, true);
}

function visitEvent(event, eventDescription, attribute, cron) {
    try {
        Tracking.startEvent(event, eventDescription, attribute, cron).then(
            function (idEvent) {
                timeEvents[event] = idEvent;
                console.log('Visited ' + event);
            },
            function (err) {
                console.error(err);
            }
        );
    } catch(err) {
        console.error(err);
    }
}

function endVisitEvent(pageId) {
    if (pageId != null && timeEvents[pageId] != null) {
        try {
            var eventId = timeEvents[pageId];
            Tracking.endEvent(eventId).then(
                function () {
                    console.log('Ended time event for page '  + pageId);
                    delete timeEvents[pageId];
                },
                function (err) {
                    console.error(err);
                }
            );
        } catch(err) {
            console.error(err);
        }
    }
}

function setMandatoryEvents(events) {
    try {
        Tracking.setMandatoryEvents(events).then(
            function () {
                console.log('Setted ' + events);
            },
            function (err) {
                console.error(err);
            } 
        );
    } catch(err) {
        console.error(err);
    }
}

function getDataEventIdFromPage(page) {
    if(page != null) {
        if(typeof page === 'string') {
            return page;
        }
        return page[0].attributes["data-md-event-id"].value;
    }
}